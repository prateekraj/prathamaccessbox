package entity;

public class MyCar {

	private int carId;
	private String modelName;
	private String reg_num;
	private String next_service_date;
	private String service_provider;
	private String insurance_expiry;
	private String insurance_provider;
	private String puc_expiry;
	private String car_photo_path;
	private String car_photo_attempt_path;
	private String insurance_document_name;
	private String puc_document_name;
	private String owner;
	private int changeSet;
	private boolean data_verified;

	private static MyCar obj = null;
	/*public MyCar(int carId, String model){
		this.carId = carId;
		this.modelName = model;
	}*/
	public String getReg_num() {
		if(reg_num!=null){
			return reg_num;
		}else return "";
	}

	public void setReg_num(String reg_num) {
		this.reg_num = reg_num;
	}

	public String getNext_service_date() {
		if(next_service_date!=null){
			return next_service_date;
		}else return "";
	}

	public void setNext_service_date(String next_service_date) {
		this.next_service_date = next_service_date;
	}

	public String getService_provider() {
		if(service_provider!=null){
			return service_provider;
		}else return "";
	}

	public void setService_provider(String service_provider) {
		this.service_provider = service_provider;
	}

	public String getInsurance_expiry() {
		if(insurance_expiry!=null){
			return insurance_expiry;
		}else return "";
	}

	public void setInsurance_expiry(String insurance_expiry) {
		this.insurance_expiry = insurance_expiry;
	}

	public String getInsurance_provider() {
		if(insurance_provider!=null){
			return insurance_provider;
		}else return "";
	}

	public void setInsurance_provider(String insurance_provider) {
		this.insurance_provider = insurance_provider;
	}

	public String getPuc_expiry() {
		if(puc_expiry!=null){
			return puc_expiry;
		}else return "";
	}

	public void setPuc_expiry(String puc_expiry) {
		this.puc_expiry = puc_expiry;
	}

	public String getCar_photo_path() {
		if(car_photo_path!=null){
			return car_photo_path;
		}else return "";
	}

	public void setCar_photo_path(String car_photo_path) {
		this.car_photo_path = car_photo_path;
	}

	public String getCar_photo_attempt_path() {
		if(car_photo_attempt_path!=null){
			return car_photo_attempt_path;
		}else return "";
	}

	public void setCar_photo_attempt_path(String car_photo_attempt_path) {
		this.car_photo_attempt_path = car_photo_attempt_path;
	}

	public String getInsurance_document_name() {
		if(insurance_document_name!=null){
			return insurance_document_name;
		}else return "";
	}

	public void setInsurance_document_name(String insurance_document_name) {
		this.insurance_document_name = insurance_document_name;
	}

	public String getPuc_document_name() {
		if(puc_document_name!=null){
			return puc_document_name;
		}else return "";
	}

	public void setPuc_document_name(String puc_document_name) {
		this.puc_document_name = puc_document_name;
	}

	public String getOwner() {
		if(owner!=null){
			return owner;
		}else return "";
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public int getCarId() {
		if(carId!=0){
			return carId;
		}else return 0;
	}

	public void setCarId(int carId) {
		this.carId = carId;
	}

	public String getModelName() {
		if(modelName!=null){
			return modelName;
		}else return "";
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public int getChangeSet() {
		return changeSet;
	}

	public void setChangeSet(int changeSet) {
		this.changeSet = changeSet;
	}

	public boolean isData_verified() {
		return data_verified;
	}

	public void setData_verified(boolean data_verified) {
		this.data_verified = data_verified;
	}

}
