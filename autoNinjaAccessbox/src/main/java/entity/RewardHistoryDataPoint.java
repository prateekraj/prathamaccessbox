package entity;

public class RewardHistoryDataPoint {
	
	
	private String _reward;
	private String _actionType;  // addition or redemption // credit / debit
	private String _totalReward; 
	private String _date;
	private String _rewardRemark;

	
	public RewardHistoryDataPoint(String reward, String actionType,
			String date, String rewardRemark) {
		super();
		this._reward = reward;
		this._actionType = actionType;
		this._date = date;
		this._rewardRemark = rewardRemark;
	}
	
	public RewardHistoryDataPoint(String totalReward) {
		super();
		this._totalReward = totalReward;
		
	}
	public String get_reward() {
		return _reward;
	}
	public void set_reward(String _reward) {
		this._reward = _reward;
	}
	public String get_actionType() {
		return _actionType;
	}
	public void set_actionType(String _actionType) {
		this._actionType = _actionType;
	}
	public String get_totalReward() {
		return _totalReward;
	}
	public void set_totalReward(String _totalReward) {
		this._totalReward = _totalReward;
	}
	public String get_date() {
		return _date;
	}
	public void set_date(String _date) {
		this._date = _date;
	}

	public String get_rewardRemark() {
		return _rewardRemark;
	}

	public void set_rewardRemark(String _rewardRemark) {
		this._rewardRemark = _rewardRemark;
	}
	
}
