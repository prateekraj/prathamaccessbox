package entity;


import myJsonData.MessagesData;
import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

public class Messages implements Parcelable {
	
	private int _uid;
	private Boolean _read = false;
	private String _subject = "";
	private String _body = "";
	private String _date = "";
	private String _img = "";
	private CallForAction _CFA = new CallForAction();

	public Messages(int uid, String sub, String body, String date, Boolean read) {
		this._uid = uid;
		this._subject = sub;
		this._body = body;
		this._date = date;
		this._read = read;
	}
	public Messages(int uid, String sub, String body, String date) {
		this(uid, sub, body, date, false);
	}
	public Messages(int uid, String sub, String body, String date, Boolean read, CallForAction cfa, String img) {
		this(uid, sub, body, date, read);
		this._CFA = cfa;
		this._img = img;
	}
	public Messages(int uid, String sub, String body, String date, Boolean read, CallForAction cfa) {
		this(uid, sub, body, date, read, cfa, "");
	}
	public Messages(int uid, String sub, String body, String date, Boolean read, String img) {
		this(uid, sub, body, date, read, new CallForAction(), img);
	}
	
	public void setReadFlag (Boolean readFlag) {
		this._read = readFlag;
	}
	public Boolean isRead () {
		return this._read;
	}
	
	public void setSubject (String sub) {
		this._subject = sub;
	}
	public String getSubject () {
		return this._subject;
	}

	public void setBody (String body) {
		this._body = body;
	}
	public String getBody () {
		return this._body;
	}

	public void setDate (String date) {
		this._date = date;
	}
	public String getDate () {
		return this._date;
	}
	
	public void setImage (String img) {
		this._img = img;
	}
	public String getImage () {
		return this._img;
	}

	public int getUid () {
		return this._uid;
	}
	
	public CallForAction getCFA() {
		return this._CFA;
	}

	public void markAsRead(Context ctxt) {
		int id = this.getUid();
		if (!this.isRead()) {
			MessagesData.markAsRead(ctxt, id);
			this.setReadFlag(true);
		}
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(_uid);
		dest.writeBooleanArray(new boolean[] {_read});
		dest.writeString(_subject);
		dest.writeString(_body);
		dest.writeString(_date);
		dest.writeParcelable(_CFA, 0);
		dest.writeString(_img);
	}
	
	private Messages(Parcel in) {
		this._uid = in.readInt();
		boolean[] b = new boolean[1];
		in.readBooleanArray(b);
		this._read = b[0];
		this._subject = in.readString();
		this._body = in.readString();
		this._date = in.readString();
		this._CFA = in.readParcelable(null);
		this._img = in.readString();
	}

	public static final Parcelable.Creator<Messages> CREATOR = new Parcelable.Creator<Messages>() {

		public Messages createFromParcel(Parcel source) {
			return new Messages(source);
		}

		public Messages[] newArray(int size) {
			return new Messages[size];
		}
	};
}
