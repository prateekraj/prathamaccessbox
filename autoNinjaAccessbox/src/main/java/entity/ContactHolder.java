package entity;

public class ContactHolder {
	private String _id;
	private String _name;
	private String _number;
	
	public ContactHolder(String id, String name, String number) {
		this._id = id;
		this._name = name;
		this._number = number;
	}

	public String getId() {
		return _id;
	}

	public String getName() {
		return _name;
	}

	public String getNumber() {
		return _number;
	}
}
