package entity;

import android.os.Parcel;
import android.os.Parcelable;

public class Tooltips implements Parcelable {
	
	private String _subject = "";
	private String _body = "";
	/*
	private String _category = "";
	public static final String CAT_Engine = "Engine";
	public static final String CAT_Fuel = "Fuel";
	public static final String CAT_Mirrors = "Mirrors";
	public static final String CAT_Tyres = "Tyres";
	public static final String CAT_Indicators = "Indicators";
	public static final String CAT_Emergency = "Emergency";
	public static final String CAT_AC_Unit = "AC Unit";
	public static final String CAT_Electrical = "Electrical";
	public static final String CAT_Brakes = "Brakes";
	*/
	private String _teaser = "";
	private String _imgs[] = {};

	public Tooltips(String sub, String body, String teaser) {
		this._subject = sub;
		this._body = body;
		//this._category = category;
		this._teaser = teaser;
		this._imgs = new String[0];
	}
	
	public Tooltips(String sub, String body, String teaser, String[] imgNames) {
		this._subject = sub;
		this._body = body;
		//this._category = category;
		this._teaser = teaser;
		this._imgs = imgNames;
	}
	
	public void setSubject (String sub) {
		this._subject = sub;
	}
	public String getSubject () {
		return this._subject;
	}

	public void setBody (String body) {
		this._body = body;
	}
	public String getBody () {
		return this._body;
	}
/*
	public void setCategory (String category) {
		this._category = category;
	}
	public String getCategory () {
		return this._category;
	}
*/
	public void setTeaser (String teaser) {
		this._teaser = teaser;
	}
	public String getTeaser () {
		return this._teaser;
	}
	
	public void setImgNames (String[] names) {
		this._imgs = names;
	}
	public String[] getImgNames () {
		return this._imgs;
	}
	
	public void setImage (String imgName, int index) {
		this._imgs[index] = imgName;
	}
	public String getImg (int index) {
		if (index < 0 || index >= _imgs.length) {
			return "";
		}
		else {
			return this._imgs[index];
		}
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(_subject);
		dest.writeString(_body);
		dest.writeString(_teaser);
		dest.writeStringArray(_imgs);
	}
	
	private Tooltips(Parcel p) {
		this(p.readString(), p.readString(), p.readString());
		
		String [] strArr = new String[] {};
		p.readStringArray(strArr);
		
		this.setImgNames(strArr);
	}
	
	public static final Parcelable.Creator<Tooltips> CREATOR = new Creator<Tooltips>() {
		
		@Override
		public Tooltips[] newArray(int size) {
			return new Tooltips[size];
		}
		
		@Override
		public Tooltips createFromParcel(Parcel source) {
			return new Tooltips(source);
		}
	};
}
