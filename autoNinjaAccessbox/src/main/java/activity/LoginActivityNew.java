package activity;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.fiveshells.prathamplus.R;
import fragment.LoginBaseFragment;
import fragment.LoginMobileFragment;
import fragment.LoginBaseFragment.LoginReplaceListener;

public class LoginActivityNew extends MyFragmentedActivityBase implements LoginBaseFragment.LoginReplaceListener {
	 
	android.support.v7.app.ActionBar actionBar;
	RelativeLayout actionBarView;
	Toolbar toolbar;
	
	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
       /* actionBar = getSupportActionBar();
		
		actionBarView = (RelativeLayout) getLayoutInflater().inflate(R.layout.actionbar_home, null);
		((ImageView) actionBarView.findViewById(R.id.burgerButton)).setVisibility(View.INVISIBLE);

		BitmapDrawable background = (BitmapDrawable) getResources().getDrawable(R.drawable.header_gradient);
		background.setTileModeX(Shader.TileMode.REPEAT);
		int sdk = android.os.Build.VERSION.SDK_INT;
		if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
			actionBarView.setBackgroundDrawable(background);
		} else {
			actionBarView.setBackground(background);
		}*/
		//actionBar.hide();

		
		//actionBar.setCustomView(actionBarView);
		
		setContentView(R.layout.activity_frag_content_only);
		toolbar = (Toolbar)findViewById(R.id.toolbar_inner);
		toolbar.setVisibility(View.GONE);
		
		replaceFragment(new LoginMobileFragment(), false);
	}
	
	public boolean onCreateOptionsMenu(Menu menu) {
		return super.onCreateOptionsMenu(menu);
	}
	
	protected void onResume() {
		super.onResume();
		
	}

	@Override
	public void onFragmentReplace(Fragment fragment, Boolean storeStack) {
		replaceFragment(fragment, storeStack);
	}
}
