package activity;

import com.fiveshells.prathamplus.R;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

public class SplashScreenActivity extends AppCompatActivity{

	// Splash screen timer
	private static int SPLASH_TIME_OUT = 2000;
	private int PERMISSION_ALL = 1;
	private String[] PERMISSIONS;
	private boolean permissionRequested = false;


	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_splash_screen);
		PERMISSIONS = new String[]{Manifest.permission.CAMERA,
				Manifest.permission.READ_CONTACTS,
				Manifest.permission.ACCESS_FINE_LOCATION,
				Manifest.permission.ACCESS_COARSE_LOCATION,
				Manifest.permission.WRITE_EXTERNAL_STORAGE,
				Manifest.permission.READ_EXTERNAL_STORAGE,
				Manifest.permission.READ_PHONE_STATE,
				Manifest.permission.CALL_PHONE,
		};

		if (hasPermissions(PERMISSIONS)) {
			System.out.println("Permission Granted 1");
			init();
		} else if(!permissionRequested) {
			System.out.println("Permission Granted 2");
			permissionRequested = true;
			ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
		}
		//getSupportActionBar().hide();
	}

	private void init() {
		System.out.println("Permission Granted 3");
		new Handler().postDelayed(new Runnable() {

			/*
			 * Showing splash screen with a timer. This will be useful when you
			 * want to show case your app logo / company
			 */

			@Override
			public void run() {
				// This method will be executed once the timer is over
				// Start your app main activity
				Intent i = new Intent(SplashScreenActivity.this, MainActivity.class);
				startActivity(i);
				finish();

			}
		}, SPLASH_TIME_OUT);
	}

	public boolean hasPermissions(String... permissions) {
		if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && permissions != null) {
			for (String permission : permissions) {
				if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
					return false;
				}
			}
		}
		return true;
	}

	public boolean isSelectedNeverAskAgainPermissions(String... permissions) {
		for (String permission : permissions) {
			if (ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_DENIED) {
				if (!ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
					return true;
				}

			}
		}
		return false;

	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		permissionRequested = false;
		if (requestCode == PERMISSION_ALL && hasPermissions(PERMISSIONS)) {
			init();
		}else {
			if(isSelectedNeverAskAgainPermissions(PERMISSIONS)){
				Intent intent = new Intent();
				intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
				Uri uri = Uri.fromParts("package", getPackageName(), null);
				intent.setData(uri);
				startActivity(intent);
				this.finish();
			}else {
				ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
			}
		}
	}
}
