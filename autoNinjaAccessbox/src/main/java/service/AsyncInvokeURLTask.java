package service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.AsyncTask;
import android.util.Log;

public class AsyncInvokeURLTask extends AsyncTask<String, Void, String> {
	public static final int REQUEST_TYPE_GET = 1;
	public static final int REQUEST_TYPE_POST = 2;
	
    private OnPostExecuteListener mPostExecuteListener = null;
    private int requestType = 0;
    private MultipartEntity postEntity = null;

    public static interface OnPostExecuteListener{
        void onPostExecute(String result);
    }

    public AsyncInvokeURLTask(OnPostExecuteListener postExecuteListener, int requestType, MultipartEntity postEntity) throws Exception {
        mPostExecuteListener = postExecuteListener;
        this.requestType = requestType;
        this.postEntity = postEntity;
        
        if (mPostExecuteListener == null)
            throw new Exception("PostExeccuteListener cannot be null.");
        else if (requestType != REQUEST_TYPE_GET && requestType != REQUEST_TYPE_POST) 
            throw new Exception("Invalid request type:" + requestType);
        else if (requestType == REQUEST_TYPE_POST && this.postEntity == null) 
        	throw new Exception("POST request cannot have a null MultipartEntity!");
    }

    AsyncInvokeURLTask(OnPostExecuteListener postExecuteListener) throws Exception {
    	this(postExecuteListener, REQUEST_TYPE_GET, null);
    }

    public AsyncInvokeURLTask(OnPostExecuteListener postExecuteListener, int requestType) throws Exception {
    	this(postExecuteListener, requestType, null);
    }

    @Override
    protected String doInBackground(String... params) {

        String result = "";

        // Create a new HttpClient and Post Header
        HttpClient httpclient = new DefaultHttpClient();
        
        if (requestType == REQUEST_TYPE_GET) {
	        HttpGet httpget = new HttpGet(params[0]);
	        
	        try {
		        HttpResponse response = httpclient.execute(httpget);
	        	//Log.d("AsyncInvokeURLTask",	response.getStatusLine().toString());
	        	
	        	HttpEntity entity = response.getEntity();
	        	
	            if (entity != null){
	                InputStream inStream = entity.getContent();
	                result = convertStreamToString(inStream).trim();
	            }
    			else {
    				//Log.d("AsyncInvokeURLTask", "http-entity is null!");
    				result = "{\"error\":\"HTTP response entity is null!\"}";
    			}
	        } catch (ClientProtocolException e) {
    			//Log.e("AsyncInvokeURLTask", e.getMessage());
    			result = "{\"error\":\"ClientProtocolException occoured!\"}";
	        } catch (IOException e) {
    			//Log.e("AsyncInvokeURLTask", e.getMessage());
    			result = "{\"error\":\"IOException occoured!\"}";
	        } catch (Exception e) {
    			//Log.e("AsyncInvokeURLTask", e.getMessage());
    			result = "{\"error\":\"Exception occoured!\"}";
	        }
        }
        else if (requestType == REQUEST_TYPE_POST) {
        	HttpPost httpPost = new HttpPost(params[0]);
        	try {
        		httpPost.setEntity(postEntity);

        		HttpResponse response = httpclient.execute(httpPost);
    			HttpEntity entity = response.getEntity();

    			if (entity != null) {
    				// A Simple JSON Response Read
    				InputStream instream = entity.getContent();
    				result = convertStreamToString(instream).trim();
    				//Log.d("AsyncInvokeURLTask", "http-entity-result : " + result);
    			}
    			else {
    				//Log.d("AsyncInvokeURLTask", "http-entity is null!");
    				result = "{\"error\":\"HTTP response entity is null!\"}";
    			}
    		} catch (ClientProtocolException e) {
    			//Log.e("AsyncInvokeURLTask", e.getMessage());
    			result = "{\"error\":\"ClientProtocolException occoured!\"}";
    		} catch (IOException e) {
    			//Log.e("AsyncInvokeURLTask", e.getMessage());
    			result = "{\"error\":\"IOException occoured!\"}";
    		}
        }
        return result;
    }

    @Override
    protected void onPostExecute(String result) {
        if (mPostExecuteListener != null){
            //JSONObject json = new JSONObject(result);
            //mPostExecuteListener.onPostExecute(json);
        	mPostExecuteListener.onPostExecute(result);
        }
    }

    private static String convertStreamToString(InputStream is){
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;

        try {
            while ((line = reader.readLine()) != null) {
				//sb.append(line + "\n");
				sb.append(line); 
				// for our purpose we do not need the line breaks in the received stream... 
				// so just append the whole as a single line => remove the (+ "\n") part
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}
