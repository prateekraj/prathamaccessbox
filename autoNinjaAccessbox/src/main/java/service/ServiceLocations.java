package service;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import config.StaticConfig;
import db.DatabaseHelper;
import db.LocationsDataSource;
import db.TableContract.LocationsDB;

public class ServiceLocations {
/*
	public static void fetchLocations(final Context context) {

		String executeURL = StaticConfig.API_FETCH_LOCATIONS;
		executeURL += "?" + StaticConfig.getCMSPass();
		//System.out.println("HOTLINES URL: " + executeURL);
		
		try {
			AsyncInvokeURLTask task = new AsyncInvokeURLTask(new AsyncInvokeURLTask.OnPostExecuteListener() {

				private JSONObject jsonObj = null;
				private JSONObject jSonText = null;
				private int new_change_set = 0;
				private JSONArray JsonLocationsArray = null;
				LocationsDataSource locDatasource = new LocationsDataSource(context);
				ContentValues cv = null;

				public void onPostExecute(String result) {
					//System.out.println("LOCATIONS:"+result);
				
					if(null!=result){
						try{
							System.out.println("LOCATIONS RESULT"+result);
							jsonObj = new JSONObject(result);
							if (jsonObj.has("error") && jsonObj.getString("error").equalsIgnoreCase("false")) {
								jSonText = jsonObj.getJSONObject("text");
								//System.out.println("change set loc"+jSonText.optString("change_set"));
								new_change_set  = Integer.parseInt(jSonText.optString("change_set"));
								JsonLocationsArray = jSonText.getJSONArray("locations");
							} else {
								jSonText = jsonObj.getJSONObject("text");
							}
							if (jsonObj.has("error")//|| StaticConfig.masterHotlineSelector.size() == 0)
									&& jsonObj.getString("error").equalsIgnoreCase("false")
									&& (locDatasource.getSavedLocChangedSet() < new_change_set) ){
								locDatasource.deleteLocDb();
								for (int i = 0; i < JsonLocationsArray.length(); i++) {
									JSONObject c = JsonLocationsArray.getJSONObject(i);
									try{
									cv = new ContentValues();
									cv.put(LocationsDB.COL_LOC_NAME, c.optString("name".trim()));
									cv.put(LocationsDB.COL_LOC_ID, Integer.parseInt(c.optString("id".trim())));
									cv.put(LocationsDB.COL_LOC_SERVICE_ENABLED, Integer.parseInt(c.optString("service_enabled".trim())));
									cv.put(LocationsDB.COL_LOC_CHAT_CEO_ENABLED, Integer.parseInt(c.optString("chat_ceo_enabled".trim())));
									cv.put(LocationsDB.COL_LOC_CHAT_ADVISOR_ENABLED, Integer.parseInt(c.optString("chat_advisor_enabled".trim())));
									cv.put(LocationsDB.COL_LOC_ENQUIRY_ENABLED, Integer.parseInt(c.optString("enquiry_enabled".trim())));
									cv.put(LocationsDB.COL_LOC_PAYMENT_ENABLED, Integer.parseInt(c.optString("payment_enabled".trim())));
									cv.put(LocationsDB.COL_LOC_TEST_DRIVE_ENABLED, Integer.parseInt(c.optString("test_drive_enabled".trim())));
									cv.put(LocationsDB.COL_LOC_TEST_DRIVE_ENABLED, Integer.parseInt(c.optString("test_drive_enabled".trim())));
									cv.put(LocationsDB.COL_LOC_CHANGE_SET, new_change_set);
									locDatasource.insertLocations(cv);
									}catch(SQLException e){
										e.printStackTrace();
									}
								}setLocations(context);
								
							}
						}catch(JSONException e){
							e.printStackTrace();
						}
					}
					}

			}, AsyncInvokeURLTask.REQUEST_TYPE_GET);
			task.execute(executeURL);
		} catch (Exception e1) {
			// Log.e(TAG, "Exception: " + e1.toString());
		}
	}*/

	public static void setLocations(Context context) {
		LocationsDataSource locDatasource = new LocationsDataSource(context);
		ArrayList<String> locChatAdvisor = locDatasource.getLocation(LocationsDB.COL_LOC_CHAT_ADVISOR_ENABLED, 1);
		ArrayList<String> locChatCeo = locDatasource.getLocation(LocationsDB.COL_LOC_CHAT_CEO_ENABLED, 1);
		ArrayList<String> locEnquiry = locDatasource.getLocation(LocationsDB.COL_LOC_ENQUIRY_ENABLED, 1);
		ArrayList<String> locPayment = locDatasource.getLocation(LocationsDB.COL_LOC_PAYMENT_ENABLED, 1);
		ArrayList<String> locService = locDatasource.getLocation(LocationsDB.COL_LOC_SERVICE_ENABLED, 1);
		ArrayList<String> loctestDrive = locDatasource.getLocation(LocationsDB.COL_LOC_TEST_DRIVE_ENABLED, 1);
		
		StaticConfig.LOCS_CHAT_ADVISOR = new String[locChatAdvisor.size()];
		StaticConfig.LOCS_CHAT_CEO = new String[locChatCeo.size()];
		StaticConfig.LOCS_ENQUIRY = new String[locEnquiry.size()];
		StaticConfig.LOCS_PAYMENT = new String[locPayment.size()];
		StaticConfig.LOCS_SERVICE_STATIONS = new String[locService.size()];
		StaticConfig.LOCS_TEST_DRIVE = new String[loctestDrive.size()];
		
		for(int i =0;i<StaticConfig.LOCS_CHAT_ADVISOR.length;i++){
			StaticConfig.LOCS_CHAT_ADVISOR[i]=locChatAdvisor.get(i);
		}
		for(int i =0;i<StaticConfig.LOCS_CHAT_CEO.length;i++){
			StaticConfig.LOCS_CHAT_CEO[i]=locChatCeo.get(i);
		}
		for(int i =0;i<StaticConfig.LOCS_ENQUIRY.length;i++){
			StaticConfig.LOCS_ENQUIRY[i]=locEnquiry.get(i);
		}
		for(int i =0;i<StaticConfig.LOCS_PAYMENT.length;i++){
			StaticConfig.LOCS_PAYMENT[i]=locPayment.get(i);
		}
		for(int i =0;i<StaticConfig.LOCS_SERVICE_STATIONS.length;i++){
			StaticConfig.LOCS_SERVICE_STATIONS[i]=locService.get(i);
		}
		for(int i =0;i<StaticConfig.LOCS_TEST_DRIVE.length;i++){
			StaticConfig.LOCS_TEST_DRIVE[i]=loctestDrive.get(i);
		}
	}
}
