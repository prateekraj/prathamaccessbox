package service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
//import android.util.Log;

public class NetworkReceiver extends BroadcastReceiver {
	//private static final String TAG = "NetworkReceiver";
	
	@Override
	public void onReceive(Context context, Intent intent) {
		boolean isNetworkDown = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
		
		if (isNetworkDown) {
			//Log.d(TAG, "onReceive NOT connected, stopping updater service");
			//Log.d(TAG, "onReceive NOT connected");
			//context.stopService(new Intent(context, MessengerService.class));
		}
		else {
			//Log.d(TAG, "onReceive connected, starting updater service");
			Intent i = new Intent(context, BackgroundService.class);
			context.startService(i);
		}
	}
}
