package adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.fiveshells.prathamplus.R;

import entity.Tooltips;

public class TooltipsAdapter extends ArrayAdapter<Tooltips> {
	Context context;
	int layoutResId;
	Tooltips tips[] = null;

	public TooltipsAdapter(Context context, int layoutResourceId, Tooltips[] objects) {
		super(context, layoutResourceId, objects);
		this.layoutResId = layoutResourceId;
		this.context = context;
		this.tips = objects;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		
		TipsHolder holder = null;
		
		if (row == null) {
			LayoutInflater lInflater = ((Activity) context).getLayoutInflater();
			
			row = lInflater.inflate(layoutResId, parent, false);
			
			holder = new TipsHolder();
			holder.tvTitle = (TextView) row.findViewById(R.id.row_list_subject);
			holder.tvTeaser = (TextView) row.findViewById(R.id.row_list_detail);
			holder.tvExtra = (TextView) row.findViewById(R.id.row_list_date);
			
			row.setTag(holder);
		}
		else {
			holder = (TipsHolder) row.getTag();
		}
		
		//row.setBackgroundColor(context.getResources().getColor(R.drawable.tooltip_row_bg));
		
		Tooltips tps = tips[position];
		holder.tvTitle.setText(tps.getSubject());
		holder.tvTeaser.setText(tps.getTeaser());
		
		holder.tvTeaser.setTextColor(context.getResources().getColor(R.color.my_orange2));
		holder.tvExtra.setVisibility(View.GONE);
		
		return row;
	}
	
	static class TipsHolder {
		TextView tvTitle;
		TextView tvTeaser;
		TextView tvExtra;
	}
}
