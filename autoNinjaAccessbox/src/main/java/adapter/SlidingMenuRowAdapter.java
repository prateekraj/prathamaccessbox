package adapter;

import java.util.ArrayList;

import com.fiveshells.prathamplus.R;

import config.ConfigInfo.Launcher;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class SlidingMenuRowAdapter extends ArrayAdapter<Launcher> {
	Context context;
	int layoutResId;
	ArrayList<Launcher> lInfo = null;

	public SlidingMenuRowAdapter(Context context, int layoutResourceId, ArrayList<Launcher> objects) {
		super(context, layoutResourceId, objects);
		this.layoutResId = layoutResourceId;
		this.context = context;
		this.lInfo = objects;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		
		RowHolder holder = null;
		
		if (row == null) {
			LayoutInflater lInflater = ((Activity) context).getLayoutInflater();
			
			row = lInflater.inflate(layoutResId, parent, false);
			
			holder = new RowHolder();
			holder.tvRow = (TextView) row.findViewById(R.id.menu_row_title);
			
			row.setTag(holder);
		}
		else {
			holder = (RowHolder) row.getTag();
		}
		
		Launcher info = lInfo.get(position);
		holder.tvRow.setText(info.getLabel());
		holder.tvRow.setCompoundDrawablesWithIntrinsicBounds(0, 0, info.getDrawableResID(), 0);
		
		return row;
	}
	
	static class RowHolder {
		TextView tvRow;
	}
}
