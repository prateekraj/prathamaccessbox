package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

public class MyIO {
	
	public static String fileToString(Context ctxt, String filename) {
		if (filename.trim().equalsIgnoreCase("")) return "";

		String ret = "";
		
		if (fileExistsInContext(ctxt, filename)) {
			try {
				FileInputStream fIn = ctxt.openFileInput(filename);
				InputStreamReader isr = new InputStreamReader(fIn);
		
				char[] inputBuffer = new char[1024];

				String readString = "";
				while (isr.read(inputBuffer) != -1) {
					readString += new String(inputBuffer);
				}
				ret = readString;

				isr.close();
			} catch (FileNotFoundException e) {
				ret = "";
			} catch (IOException e) {
				ret = "";
			}
		}
		else {
			ret = "";
		}
		
		return ret;
	}
	
	public static boolean fileExistsInContext(Context ctxt, String filename) {
		File file = ctxt.getFileStreamPath(filename);

		return file.exists();
	}
	
	public static boolean writeStringToFile(Context ctxt, String filename, String data) {
		try {
			FileOutputStream fOut = ctxt.openFileOutput(filename, Context.MODE_PRIVATE);
			OutputStreamWriter osw = new OutputStreamWriter(fOut); 

			// Write the string to the file
			osw.write(data);
			osw.flush();
			osw.close();

			return true;
		}
		catch (FileNotFoundException e) {
		}
		catch (IOException e) {
		}
		
		return false;
	}

	public static JSONArray readJSONArrayFromFile (Context ctxt, String fileName, String arrayKeyName) {
		JSONArray ret = new JSONArray();

		String readString = MyIO.fileToString(ctxt, fileName);
		
		if (!readString.trim().equalsIgnoreCase("")) {
			try {
				JSONObject jso = new JSONObject(readString);

				ret = jso.getJSONArray(arrayKeyName);
			} catch (JSONException e) {
				//Log.e(TAG, "JSONException: " + e.toString());
			}
		}
		else {
			//Log.d(TAG, "file doesnt exists");
		}
		return ret;
	}

	public static boolean writeJSONArrayToFile (Context ctxt, String fileName, JSONArray jsonArray, String arrayKeyName) {
		try {
			//save file
			
			JSONObject finalJSONObj = new JSONObject();
			finalJSONObj.put(arrayKeyName, jsonArray);

			// Write the string to the file
			return MyIO.writeStringToFile(ctxt, fileName, finalJSONObj.toString());
			
		} catch (JSONException e) {
			//Log.e(TAG, "JSONException: " + e.toString());
		}
		return false;
	}
}
