package utils;

import com.google.analytics.tracking.android.CampaignTrackingReceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class CampaignBroadCastReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		Bundle b = intent.getExtras();
		
		new CampaignTrackingReceiver().onReceive(context, intent);

	}

}
