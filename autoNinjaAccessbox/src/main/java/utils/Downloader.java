package utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.os.AsyncTask;

public class Downloader extends AsyncTask<String, Void, Void>{
	private UICallback callback;
	 InputStream in=null;
	
	public Downloader(UICallback callback){
		this.callback = callback;
	}
	/*public static void DownloadFile(UICallback callback) {
        try {
           // new Downloader(callback).execute(fileURL,directory);
            FileOutputStream f = new FileOutputStream(directory);
            URL u = new URL(fileURL);
            HttpURLConnection c = (HttpURLConnection) u.openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(true);
            c.connect();

            InputStream in = c.getInputStream();

            byte[] buffer = new byte[1024];
            int len1 = 0;
            while ((len1 = in.read(buffer)) > 0) {
                f.write(buffer, 0, len1);
            }
            f.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }*/

	@Override
	protected Void doInBackground(String... params) {
		 try {
			 	File file = new File(params[1]);
	            FileOutputStream f = new FileOutputStream(file);
	            URL u = new URL(params[0]);
	            HttpURLConnection c = (HttpURLConnection) u.openConnection();
	            c.setRequestMethod("GET");
	            c.setDoOutput(true);
	            c.connect();
	            in = c.getInputStream();
	            byte[] buffer = new byte[1024];
	            int len1 = 0;
	            while ((len1 = in.read(buffer)) > 0) {
	                f.write(buffer, 0, len1);
	            }
	            f.close();
	        } catch (Exception e) {
	            e.printStackTrace();
	        }

		return null;
	}
	 @Override
	    protected void onPostExecute(Void response) {
	        super.onPostExecute(response);
	        	if(null!=in){
	        		callback.OnUICallback(true);
	        	}else{
	        		callback.OnUICallback(false);
	        	}
	    }

}
