package fragment;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import service.AsyncInvokeURLTask;
import service.ServiceLocations;
import utils.Utils;
import myJsonData.MyJSONData;
import com.fiveshells.prathamplus.R;
import config.StaticConfig;
import googleAnalytics.GATrackerMaps;
import dialog.MyPickerDialogFrag;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class EnquiryFragment extends MyHotlineMultiSelectFragment 
					implements View.OnClickListener, MyPickerDialogFrag.onSendResultListener {

	
	TextView tvEnquiryFor;
	TextView tvLocationEnquiry;
	TextView tvSendMsgButton;
	TextView tvEnquiryCallButton;
	TextView tvNoData;
	LinearLayout datll;
	
	EditText edtQueryBox;
	
	ProgressDialog progressDialog;
	private String enquiryFor = "";
	private String enquiry_loc = "";
	private static final int REQUEST_CODE_ENQUIRY_TYPE_CHOICE = 55;
	private static final int REQUEST_CODE_WORKSHOP_CENTER_CHOICE = 66;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		tracker.send(GATrackerMaps.VIEW_ENQUIRY);
		
		View v = inflater.inflate(R.layout.frag_enquiry_home, container, false);
		tvEnquiryFor = (TextView) v.findViewById(R.id.enquiry_about_tv);
		tvLocationEnquiry = (TextView) v.findViewById(R.id.enquiry_location_tv);
		tvSendMsgButton = (TextView) v.findViewById(R.id.enquiry_send_message_btn);
		tvEnquiryCallButton = (TextView) v.findViewById(R.id.enquiry_by_call_button);
		tvNoData = (TextView) v.findViewById(R.id.textNoLocsType);
		datll = (LinearLayout) v.findViewById(R.id.dataLL);
		if (null == StaticConfig.LOCS_ENQUIRY || StaticConfig.LOCS_ENQUIRY.length == 0) {
			 ServiceLocations.setLocations(getActivity());
		 }
		if(StaticConfig.LOCS_ENQUIRY == null || StaticConfig.ENQUIRY_TYPE_LIST==null ||StaticConfig.LOCS_ENQUIRY.length==0 || StaticConfig.ENQUIRY_TYPE_LIST.length==0)
		{
			tvNoData.setVisibility(View.VISIBLE);
			tvNoData.setText(R.string.try_msg);
			datll.setVisibility(View.GONE);
		}else{
			tvNoData.setVisibility(View.GONE);
			datll.setVisibility(View.VISIBLE);	
		}
		edtQueryBox = (EditText) v.findViewById(R.id.enquiry_edit_message);
		if (StaticConfig.LOCS_ENQUIRY != null && StaticConfig.LOCS_ENQUIRY.length == 1) {
			tvLocationEnquiry.setVisibility(View.GONE);
		} /*else if (StaticConfig.LOCS_ENQUIRY == null || StaticConfig.LOCS_ENQUIRY.length == 0) {
			Toast.makeText(getActivity(), R.string.try_msg, Toast.LENGTH_LONG).show();
		}*/
		if (StaticConfig.ENQUIRY_TYPE_LIST != null && StaticConfig.ENQUIRY_TYPE_LIST.length == 1) {
			tvEnquiryFor.setVisibility(View.GONE);
		} else if (StaticConfig.ENQUIRY_TYPE_LIST != null && StaticConfig.ENQUIRY_TYPE_LIST.length > 1) {
			tvEnquiryFor.setVisibility(View.VISIBLE);
		}

		tvEnquiryFor.setOnClickListener(this);
		tvLocationEnquiry.setOnClickListener(this);
		tvSendMsgButton.setOnClickListener(this);
		return v;
	}
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public void onResume() {
		if (tvEnquiryFor.getVisibility() == View.GONE) {
			toolbarListener.setText(StaticConfig.ENQUIRY_TYPE_LIST[0] + " Enquiry");
		} else {
			toolbarListener.setText("Enquiry");
		}

		super.onResume();
	}

	@Override
	public void onClick(View v) {
		DialogFragment pickerFragDialog;
		switch (v.getId()) {
		case R.id.enquiry_about_tv:
			tracker.send(GATrackerMaps.EVENT_ENQUIRY_ABOUT_DIALOG);
			pickerFragDialog = new MyPickerDialogFrag().newInstance(MyPickerDialogFrag.TYPE_ENQUIRY_LIST_PICKER);
			pickerFragDialog.setTargetFragment(this, REQUEST_CODE_ENQUIRY_TYPE_CHOICE);
			pickerFragDialog.show(getActivity().getSupportFragmentManager(), "enquiryListPicker");
			break;
			
		case (R.id.enquiry_location_tv):
			tracker.send(GATrackerMaps.EVENT_ENQUIRY_LOCATION_DIALOG);
			pickerFragDialog = MyPickerDialogFrag.newInstance(MyPickerDialogFrag.TYPE_ENQUIRY_LOC_LIST_PICKER);
			pickerFragDialog.setTargetFragment(this, REQUEST_CODE_WORKSHOP_CENTER_CHOICE);
			pickerFragDialog.show(getActivity().getSupportFragmentManager(), "workshoplistPicker");
			break; 
			
		case R.id.enquiry_send_message_btn:
			tracker.send(GATrackerMaps.EVENT_ENQUIRY_TEXT_SEND);
			if (tvEnquiryFor.getVisibility() == View.VISIBLE && tvEnquiryFor.getText().toString().trim().equalsIgnoreCase("")) {
				Toast.makeText(getActivity(), "Please fill all fields before proceeding!", Toast.LENGTH_LONG).show();
				return;
			} else if (tvEnquiryFor.getVisibility() == View.VISIBLE && !tvEnquiryFor.getText().toString().trim().equalsIgnoreCase("")) {
				enquiryFor = tvEnquiryFor.getText().toString().trim();
			} else if (tvEnquiryFor.getVisibility() == View.GONE) {
				enquiryFor = StaticConfig.ENQUIRY_TYPE_LIST[0];
			}
			if (tvLocationEnquiry.getVisibility()==View.VISIBLE && !tvLocationEnquiry.getText().toString().equalsIgnoreCase("")) {
				enquiry_loc = tvLocationEnquiry.getText().toString().trim();
			}else if(tvLocationEnquiry.getVisibility()==View.GONE){
				enquiry_loc = StaticConfig.LOCS_ENQUIRY[0];
			}
				if (edtQueryBox.getText().toString().trim().equalsIgnoreCase("")) {
					Toast.makeText(getActivity(), "Your query box is empty", Toast.LENGTH_LONG).show();
					return;
			}
				// String message = edtQueryBox.getText().toString();
				if (Utils.isNetworkConnected(getActivity())) {
					progressDialog = ProgressDialog.show(getActivity(), "Sending your Query", "Please wait for a moment");
					progressDialog.setCancelable(true);

					MyJSONData own = new MyJSONData(getActivity(), MyJSONData.TYPE_OWN);

					try {
						String params = "?phone=" + StaticConfig.LOGGED_PHONE_NUMBER;
						params += "&enquiry_about=" + URLEncoder.encode(enquiryFor.trim(), "UTF-8");
						params += "&query=" + URLEncoder.encode(edtQueryBox.getText().toString().trim(), "UTF-8");

						if (!enquiry_loc.equalsIgnoreCase("")) {
							params += "&enquiry_loc=" + URLEncoder.encode(enquiry_loc.trim(), "UTF-8");
						} /*else if (StaticConfig.LOCS_ENQUIRY != null && StaticConfig.LOCS_ENQUIRY.length == 1) {
							params += "&enquiry_loc=" + URLEncoder.encode(StaticConfig.LOCS_ENQUIRY[0].trim(), "UTF-8");
						} else {
							Toast.makeText(getActivity(), R.string.try_msg, Toast.LENGTH_LONG).show();
						}*/
						params += "&" + StaticConfig.getCMSPass();
						Log.d("", "Enquiry :- query url ------- " + params);

						AsyncInvokeURLTask task = new AsyncInvokeURLTask(new AsyncInvokeURLTask.OnPostExecuteListener() {

							@Override
							public void onPostExecute(String result) {
								progressDialog.dismiss();
								JSONObject responseObject;
								try {
									responseObject = new JSONObject(result);
									if (responseObject.has("text")) {
										Toast.makeText(getActivity(), responseObject.getString("text"), Toast.LENGTH_LONG).show();
									} else {
										Toast.makeText(getActivity(), R.string.try_msg, Toast.LENGTH_LONG).show();
									}

								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

							}
						}, AsyncInvokeURLTask.REQUEST_TYPE_GET);

						String url = StaticConfig.API_ENQUIRY + params;
						task.execute(url);
						tvEnquiryFor.setText("");
						edtQueryBox.setText("");
						tvLocationEnquiry.setText("");
					} catch (UnsupportedEncodingException e) {
						// ignore
					} catch (Exception e) {
						// ignore
					}
				}
			}
	}
	
	@Override
	public void onSendResult(String setString, int requestCode) {
		switch (requestCode) {
		case (REQUEST_CODE_ENQUIRY_TYPE_CHOICE): 
			tvEnquiryFor.setText(setString);
			break;
		case (REQUEST_CODE_WORKSHOP_CENTER_CHOICE):
			tvLocationEnquiry.setText(setString);
		}
	}
}
