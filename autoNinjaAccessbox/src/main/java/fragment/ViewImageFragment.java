package fragment;

import java.io.File;

import com.fiveshells.prathamplus.R;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
@SuppressLint("ValidFragment")
public class ViewImageFragment  extends MyFragment {
	String bitmap = "";
	@SuppressLint("ValidFragment")
	public ViewImageFragment(String bitmap) {
		this.bitmap = bitmap;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		//Log.d("TestFragment2", "OnCreateView Called!");
		View v = inflater.inflate(R.layout.view_image, container, false);
		ImageView viewImage = (ImageView) v.findViewById(R.id.viewAnyImage);
		File thumbFile = new File(ChatAdvisorFragment.getAlbumDir(), this.bitmap);
		Bitmap bm = null;
		if(null!=BitmapFactory.decodeFile(thumbFile.getAbsolutePath())){
			 bm = BitmapFactory.decodeFile(thumbFile.getAbsolutePath());
		}else{
			bm = BitmapFactory.decodeFile(this.bitmap);
		}
		
		toolbarListener.setText("View Image");
		viewImage.setImageBitmap(bm);
		return v;
	}

}
