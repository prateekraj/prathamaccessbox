/*
package fragment;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import myJsonData.MyJSONData;

import org.json.JSONException;
import org.json.JSONObject;


import service.PaymentGetBill;
import utils.Utils;

import com.citrus.card.Card;
import com.citrus.mobile.Callback;
import com.citrus.mobile.Config;
import com.citrus.netbank.Bank;
import com.citrus.payment.Bill;
import com.citrus.payment.PG;
import com.citrus.payment.UserDetails;
import com.fiveshells.prathamplus.R;

import config.StaticConfig;
import db.UserDetailsDataSource;
import activity.PaymentWebPage;
import android.util.Log;
import android.view.View.OnClickListener;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class PaymentOptionsFragment extends MyFragment {
	
	RadioGroup rPaymentGroup;
	
	RadioButton cardCreditPayment, cardDebitPayment, bankPayment;
	
	RelativeLayout creditHolder, debitHoder; 
	
	LinearLayout netBankHolder;
	
	JSONObject customer;
	
	Button btnPayNow;
	
	TextView tvAmountCredit, tvAmountDebit, tvAmountBankPay;
	 
	EditText edtCardNoCredit, edtCvvCredit, edtCardNameCredit, edtMonthCredit, edtYearCredit, edtAmountCredit;
	
	EditText edtCardNoDebit, edtCvvDebit, edtCardNameDebit, edtMonthDebit, edtYearDebit, edtAmountDebit;
	
	EditText editChooseBank, edtAmountBankPay;
	
	Spinner spinnerBankPay;
	
	String bankCode = "";
	
	MyJSONData myOwn;
	String paymentId = "-1";
	
	String amountToPay = "";
	ProgressDialog progress;
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.frag_payment_option, container, false);
		
		init();
		
		rPaymentGroup = (RadioGroup) v.findViewById(R.id.radioPayment);
		cardCreditPayment = (RadioButton) v.findViewById(R.id.radioCredit);
		cardDebitPayment = (RadioButton) v.findViewById(R.id.radioDebit);
		bankPayment = (RadioButton) v.findViewById(R.id.radioNetBank);
		
		creditHolder = (RelativeLayout) v.findViewById(R.id.payment_credit_card_holder);
		debitHoder = (RelativeLayout) v.findViewById(R.id.payment_debit_card_holder);
		netBankHolder = (LinearLayout) v.findViewById(R.id.payment_net_banking_holder);
		
		btnPayNow = (Button) v.findViewById(R.id.btnPayNow);
		
		customer = new JSONObject();
		
		// Input(EditText) for CreditCard payment
		edtCardNoCredit = (EditText) v.findViewById(R.id.editcardnocredit);
		edtCvvCredit = (EditText) v.findViewById(R.id.editcvvcredit);
	    edtCardNameCredit = (EditText) v.findViewById(R.id.editnameoncardcredit);
		edtMonthCredit = (EditText) v.findViewById(R.id.editmonthexpcredit);
		edtYearCredit = (EditText) v.findViewById(R.id.edityearexpcredit);
		tvAmountCredit = (TextView) v.findViewById(R.id.editamountpaycardcredit);
		edtCardNoCredit.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				String creditString = edtCardNoCredit.getText().toString();
				if(creditString.length() >= 2){
				String desiredString = creditString.substring(0,2);
				if(desiredString.equalsIgnoreCase("37") || desiredString.equalsIgnoreCase("34")){
					edtCvvCredit.setFilters(new InputFilter[]{new InputFilter.LengthFilter(4)});
				}else{
					edtCvvCredit.setFilters(new InputFilter[]{new InputFilter.LengthFilter(3)});
				}
				}else{
					
				}
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});

		// Input(EditText) for DebitCard payment
		edtCardNoDebit = (EditText) v.findViewById(R.id.editcardnodebit);
		edtCvvDebit = (EditText) v.findViewById(R.id.editcvvdebit);
	    edtCardNameDebit = (EditText) v.findViewById(R.id.editnameoncarddebit);
		edtMonthDebit = (EditText) v.findViewById(R.id.editmonthexpdebit);
		edtYearDebit = (EditText) v.findViewById(R.id.edityearexpdebit);
		tvAmountDebit = (TextView) v.findViewById(R.id.editamountpaycarddebit);
		edtCardNoDebit.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				String debitString = edtCardNoDebit.getText().toString();
				if(debitString.length() >= 2){
				String desiredString = debitString.substring(0,2);
				if(desiredString.equalsIgnoreCase("37") || desiredString.equalsIgnoreCase("34")){
					edtCvvDebit.setFilters(new InputFilter[]{new InputFilter.LengthFilter(4)});
				}else{
					edtCvvDebit.setFilters(new InputFilter[]{new InputFilter.LengthFilter(3)});
				}
				}else{
					
				}
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
	

		// Input(EditText) for Net Bank
		editChooseBank = (EditText) v.findViewById(R.id.editcidno);
		tvAmountBankPay = (TextView) v.findViewById(R.id.editamountpaybank);
		spinnerBankPay = (Spinner) v.findViewById(R.id.spinnernetbank);
		
		
		// progress dialog
		progress = new ProgressDialog(getActivity());
		progress.setMessage("Loading....");
		progress.setCancelable(false);
		progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				
				
		myOwn = new MyJSONData(getActivity(), MyJSONData.TYPE_OWN);
		amountToPay = myOwn.fetchData(MyJSONData.FIELD_OWN_PAYMENT_AMOUNT);
		System.out.println(amountToPay);
		tvAmountCredit.setText(amountToPay);
		tvAmountDebit.setText(amountToPay);
		tvAmountBankPay.setText(amountToPay);
		rPaymentGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				
				switch (checkedId)
		    	{
		    	case R.id.radioCredit :
		    		creditHolder.setVisibility(View.VISIBLE);
		    		debitHoder.setVisibility(View.GONE);
		    		netBankHolder.setVisibility(View.GONE);
		    		
		    			btnPayNow.setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								if(Utils.isNetworkConnected(getActivity())){
							if(!edtCardNoCredit.getText().toString().trim().equalsIgnoreCase("") && !edtCvvCredit.getText().toString().trim().equalsIgnoreCase("")
				    				&& !edtCardNameCredit.getText().toString().equalsIgnoreCase("") && !edtMonthCredit.getText().toString().trim().equalsIgnoreCase("")
				    				&& !edtYearCredit.getText().toString().trim().equalsIgnoreCase("") && !tvAmountBankPay.getText().toString().trim().equalsIgnoreCase("")) {
								
								float value;
								tvAmountCredit.setText(amountToPay);
								try {
									
									 value = Float.parseFloat(amountToPay);
									if ((value) < 1) {
						                Toast.makeText(getActivity(), "Amount should not be less than Rs 1.00", Toast.LENGTH_LONG).show();
						                return;
						            }
								}
								catch (NumberFormatException e){
									Toast.makeText(getActivity(), "Please enter a valid payment amount!", Toast.LENGTH_LONG).show();
									return;
								}
								progress.show();
								new PaymentGetBill(StaticConfig.API_BILL_GENERATOR_URL, value, new Callback() {
									
									@Override
									public void onTaskexecuted(String bill, String error) {
										// TODO Auto-generated method stub
										Bill bills = new Bill(bill);
										Card card = new Card(edtCardNoCredit.getText().toString().trim(), edtMonthCredit.getText().toString().trim(), 
												edtYearCredit.getText().toString().trim(), edtCvvCredit.getText().toString().trim(), edtCardNameCredit.getText().toString(), "credit");
										UserDetails userDetails = new UserDetails(customer);
										PG paymentgateway = new PG(card, bills, userDetails);
										paymentgateway.setCustomParameters(getCustomParameters());
										paymentgateway.charge(new Callback() {
								            @Override
								            public void onTaskexecuted(String success, String error) {
								                processresponse(success, error);
								                progress.dismiss();
								            }
								        });
									}
								}).execute();
							} else {
					    			Toast.makeText(getActivity(), "Please fill all fields", 0).show();
							}
						 }
							else {
								Toast.makeText(getActivity(), getResources().getString(R.string.network_msg), Toast.LENGTH_LONG).show();
							}
						}
					});
	    			edtCardNoCredit.setText("");
					edtCvvCredit.setText("");
					edtCardNameCredit.setText("");
					edtMonthCredit.setText("");
					edtYearCredit.setText("");
			    	break;
		    	case R.id.radioDebit :
		    		creditHolder.setVisibility(View.GONE);
		    		debitHoder.setVisibility(View.VISIBLE);
		    		netBankHolder.setVisibility(View.GONE);
		    		
		    			btnPayNow.setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View v) {
								
						if(Utils.isNetworkConnected(getActivity())){	
							if(!edtCardNoDebit.getText().toString().trim().equalsIgnoreCase("") && !edtCvvDebit.getText().toString().trim().equalsIgnoreCase("")
				    				&& !edtCardNameDebit.getText().toString().equalsIgnoreCase("") && !edtMonthDebit.getText().toString().trim().equalsIgnoreCase("")
				    				&& !edtYearDebit.getText().toString().trim().equalsIgnoreCase("") && !tvAmountDebit.getText().toString().trim().equalsIgnoreCase("")) {
								
								float value1;
								try {
									 value1 = Float.parseFloat(amountToPay);
									if ((value1) < 1) {
						                Toast.makeText(getActivity(), "Amount should not be less than Rs 1.00", Toast.LENGTH_LONG).show();
						                return;
						            }
								}
								catch (NumberFormatException e){
									Toast.makeText(getActivity(), "Please enter a valid payment amount!", Toast.LENGTH_LONG).show();
									return;
								}
								
								progress.show();
								new PaymentGetBill(StaticConfig.API_BILL_GENERATOR_URL, value1, new Callback() {
									
									@Override
									public void onTaskexecuted(String bill, String error) {
										// TODO Auto-generated method stub
										Bill bills = new Bill(bill);
										Card card = new Card(edtCardNoDebit.getText().toString().trim(), edtMonthDebit.getText().toString().trim(), 
												edtYearDebit.getText().toString().trim(), edtCvvDebit.getText().toString().trim(), edtCardNameDebit.getText().toString(), "debit");
										UserDetails userDetails = new UserDetails(customer);
										PG paymentgateway = new PG(card, bills, userDetails);
										paymentgateway.setCustomParameters(getCustomParameters());
										paymentgateway.charge(new Callback() {
								            @Override
								            public void onTaskexecuted(String success, String error) {
								                processresponse(success, error);
								                progress.dismiss();
								            }
								        });
									}
								}).execute();
							} else {
				    			Toast.makeText(getActivity(), "Please fill all fields", 0).show();
						  }
						}
							else {
								Toast.makeText(getActivity(), getResources().getString(R.string.network_msg), Toast.LENGTH_LONG).show();
							}
					  }		
					});
	    			edtCardNoDebit.setText("");
					edtCvvDebit.setText("");
					edtCardNameDebit.setText("");
					edtMonthDebit.setText("");
					edtYearDebit.setText("");
			    	break;
		    	case R.id.radioNetBank :
		    		creditHolder.setVisibility(View.GONE);
		    		debitHoder.setVisibility(View.GONE);
		    		netBankHolder.setVisibility(View.VISIBLE);
		    //		final String amountString = edtAmountBankPay.getText().toString().trim();
		    		final String[] bankName = {
		    				"Andhra Bank", "AXIS Bank","Bank of India", "Bank Of Baroda", "Bank of Maharashtra", "Canara Bank",
		    				"Catholic Syrian Bank", "Central Bank of India", "CITI Bank", "Corporation Bank", "City Union Bank",
		    				"DEUTSCHE Bank", "Federal Bank", "HDFC Bank", "ICICI Bank", "IDBI Bank", "Indian Bank", "Indian Overseas Bank",
		    				"Induslnd Bank", "ING VYSA", "Karnataka Bank", "Kotak Mahindra Bank", "Karur Vysya Bank", "PNB Retail",
		    				"PNB Corporate", "SBI Bank", "State Bank of Bikaner and Jaipur", "State Bank of Hyderabad", "State Bank of Mysore",
		    				"State Bank of Travancore", "State Bank of Patiala", "Union Bank Of India", "United Bank of India", "Vijaya Bank",
		    				"YES Bank", "Cosmos Bank", "UCO Bank", "Select Bank"};
		    		
		    	    final String[] bankId = {"CID016", "CID002", "CID019", "CID046", "CID021", "CID051", "CID045", "CID023", "CID003", "CID025",
		    	    		"CID024", "CID006", "CID009", "CID010", "CID001", "CID011", "CID008", "CID027", "CID028", "CID029", "CID031", "CID033",
		    	    		"CID032", "CID044", "CID036", "CID005", "CID013", "CID012", "CID014", "CID015", "CID043", "CID007", "CID041", "CID042",
		    	    		"CID004", "CID053", "CID070", "000000"};
		    	    ArrayAdapter<String> aa = new ArrayAdapter<String>(getActivity(),R.layout.spinner_layout, bankName);
				    spinnerBankPay.setAdapter(aa);
				    spinnerBankPay.setSelection(bankName.length-1);
				    spinnerBankPay.setOnItemSelectedListener(new OnItemSelectedListener() {

						@Override
						public void onItemSelected(AdapterView<?> parent,
								View view, int position, long id) {
			//				String nBank = parent.getItemAtPosition(position).toString();
							
							bankCode = bankId[position];
							
						}
						@Override
						public void onNothingSelected(AdapterView<?> parent) {
							
						}
					});
		 
		    			btnPayNow.setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View v) {
						if(Utils.isNetworkConnected(getActivity())){		
							if(!tvAmountBankPay.getText().toString().trim().equalsIgnoreCase("") && !bankCode.equalsIgnoreCase("") && !bankCode.equalsIgnoreCase("000000")) {
								float value2;
								try {
									 value2 = Float.parseFloat(amountToPay);
									if ((value2) < 1) {
						                Toast.makeText(getActivity(), "Amount should not be less than Rs 1.00", Toast.LENGTH_LONG).show();
						                return;
						            }
								}
								catch (NumberFormatException e){
									Toast.makeText(getActivity(), "Please enter a valid payment amount!", Toast.LENGTH_LONG).show();
									return;
								}
								
								progress.show();
								new PaymentGetBill(StaticConfig.API_BILL_GENERATOR_URL, value2, new Callback() {
									
									@Override
									public void onTaskexecuted(String bill, String error) {
										if (TextUtils.isEmpty(error)) {
											Bill bills = new Bill(bill);
											Bank netbank = new Bank(bankCode);

									        UserDetails userDetails = new UserDetails(customer);

									        PG paymentgateway = new PG(netbank, bills, userDetails);
									        
									        paymentgateway.setCustomParameters(getCustomParameters());

									        paymentgateway.charge(new Callback() {
									            @Override
									            public void onTaskexecuted(String success, String error) {
									                processresponse(success, error);
									                progress.dismiss();
									            }
									        });
				                        }										
									}
								}).execute();
							} else {
				    			Toast.makeText(getActivity(), "Please select a bank of your choice", 0).show();
							}
						  }
							else {
								Toast.makeText(getActivity(), getResources().getString(R.string.network_msg), Toast.LENGTH_LONG).show();
							}
						}
					});
		    			*/
/*MyJSONData.editMyData(getActivity(),
								new String[] {MyJSONData.FIELD_OWN_PAYMENT_AMOUNT}, 
								new String[] {null}, MyJSONData.TYPE_OWN);*//*

			    	break;
				}
			}
		});
		
		filluserDetails();
		
		return v;
	}
	
	private void init() {
		Config.setEnv("production"); // replace it with production when you are ready

	*/
/*	Config.setupSignupId("test-signup");
		Config.setupSignupSecret("c78ec84e389814a05d3ae46546d16d2e");

		Config.setSigninId("test-signin");
		Config.setSigninSecret("52f7e15efd4208cf5345dd554443fd99");*//*

		
		*/
/*Config.setupSignupId("uaq8agl4pe-signup");
		Config.setupSignupSecret("68429aab6c6fb06aabffff73c39471fc");

		Config.setSigninId("uaq8agl4pe-signin");
		Config.setSigninSecret("d3b57f4f85f32ac07434e23159c60405");*//*

		
		Config.setupSignupId("dl9kdnqnut-signup");
		Config.setupSignupSecret("3dbd0b468da7a1e222e8bdc43ad74fb8");

		Config.setSigninId("dl9kdnqnut-signin");
		Config.setSigninSecret("4008a122f48b4bd47b369b9fba1ec330");
		
		
		
	}



	@Override
	public void onResume() {
		super.onResume();
		
		toolbarListener.setText("Payment Options");
	}
	
	private void processresponse(String response, String error) {

        if (!TextUtils.isEmpty(response)) {
            try {

                JSONObject redirect = new JSONObject(response);
                Intent i = new Intent(getActivity(), PaymentWebPage.class);

                if (!TextUtils.isEmpty(redirect.getString("redirectUrl"))) {

                    i.putExtra("url", redirect.getString("redirectUrl"));
                    startActivity(i);
                    // Using OnbackPresses bcoz after calling PaymentWebPage.class this fragment should not appear.
                    getActivity().onBackPressed();
                }
                else {
                    Toast.makeText(getActivity(), response, Toast.LENGTH_LONG).show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        else {
            Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
        }

    }
	
	private void filluserDetails() {
        */
/*All the below mentioned parameters are mandatory - missing anyone of them may create errors
        * Do not change the key in the json below - only change the values*//*

		UserDetailsDataSource  userDetailDataSource = new UserDetailsDataSource(getActivity());
		String emailId = myOwn.fetchData(MyJSONData.FIELD_OWN_EMAIL_ID);
		long mobile = userDetailDataSource.getUserOwnNumber();
		try {
			customer.put("firstName", "Customer");
			customer.put("lastName", "Technologies");
			customer.put("email", emailId);
			customer.put("mobileNo", ""+ mobile);
			customer.put("street1", "80 ft road");
			customer.put("street2", "koramangala");
			customer.put("city", "Bangalore");
			customer.put("state", "Karnataka");
			customer.put("country", "India");
			customer.put("zip", "560035");
		} catch (JSONException e) {
			e.printStackTrace();
		}

    }
	
	// Using this method to pass a custom parameter to identify the dealer
	
	private JSONObject getCustomParameters() {
		if(!myOwn.fetchData(MyJSONData.FIELD_OWN_PAYMENT_ID).equalsIgnoreCase("")) {
			paymentId = myOwn.fetchData(MyJSONData.FIELD_OWN_PAYMENT_ID);
		}else {
			paymentId = "-1";
		}
		JSONObject customParameters = new JSONObject();
			try {
				 customParameters.put("MerchantName", StaticConfig.DEALER_CUSTOM_PARAMETER_FOR_CITRUS);
				 customParameters.put("PaymentId", paymentId);
		//		 customParameters.put("customparam2", "10000");
			} catch (JSONException e) {
				 e.printStackTrace();
			}
			return customParameters;
		}

}

*/
