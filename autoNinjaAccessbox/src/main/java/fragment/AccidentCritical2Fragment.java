package fragment;

import googleAnalytics.GATrackerMaps;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.fiveshells.prathamplus.R;
import config.StaticConfig;

public class AccidentCritical2Fragment extends MyHotlineMultiSelectFragment 
				implements View.OnClickListener {
	
	TextView buttonCallMOS;
	TextView buttonCallTowing;
	TextView buttonNext;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		//Log.d("TestFragment", "OnCreateView Called!");
		View v = inflater.inflate(R.layout.frag_accident_crit2, container, false);
		
		buttonCallMOS = (TextView) v.findViewById(R.id.acc_crit2_button_call_mos);
		buttonCallTowing = (TextView) v.findViewById(R.id.acc_crit2_button_call_wheels_stuck);
		buttonNext = (TextView) v.findViewById(R.id.button_next);
		
		buttonCallMOS.setOnClickListener(this);
		buttonCallTowing.setOnClickListener(this);
		buttonNext.setOnClickListener(this);
		
		buttonCallMOS.setText("On-Road Assistance");
		buttonCallTowing.setText(StaticConfig.DEALER_FIRST_NAME + " Towing Service");
		
		return v;
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity); //call super first before doing anything else!
		
		toolbarListener.setText("Critical Situation");
		//setActionBarBackEnabled(true);
		
		//Log.d("AccidentBaseFragment", "OnAttach Called!");
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		tracker.send(GATrackerMaps.VIEW_ACCIDENT_CRITICAL2);
	}
	
	@Override
	public void onClick(View v) {
		boolean callNow = false;
		boolean openPicker = false;
		Intent i = new Intent(Intent.ACTION_CALL);
		Uri callData = null;
		DialogFragment pickerFragDialog = null;
		
		switch(v.getId()) {
		case R.id.acc_crit2_button_call_mos:
			tracker.send(GATrackerMaps.EVENT_ACCIDENT2_CALL_ONROAD_SERVICE);
			
			if (StaticConfig.ONROAD_SERVICE_SELECTOR != null && !StaticConfig.ONROAD_SERVICE_SELECTOR.isEmpty()) {
				if (StaticConfig.ONROAD_SERVICE_SELECTOR.size() == 1) {
					callData = Uri.parse("tel:" + StaticConfig.ONROAD_SERVICE_SELECTOR.get(0).getNumber());
					callNow = true;
				}
				else {
					if(!HotlinePickerDialogFrag.isPickerDialogOpen()){
						pickerFragDialog = HotlinePickerDialogFrag.newInstance("On Road Service", StaticConfig.ONROAD_SERVICE_SELECTOR);
						openPicker = true;
					}
				}
			}
			else if(!StaticConfig.HOTLINE_FETCHED){
				Toast.makeText(getActivity(), "Fetching hotlines.. please wait", Toast.LENGTH_SHORT).show();
			}
			break;
		case R.id.acc_crit2_button_call_wheels_stuck:
			tracker.send(GATrackerMaps.EVENT_ACCIDENT2_CALL_TOWING);
			
			if (StaticConfig.TOWING_SELECTOR != null && !StaticConfig.TOWING_SELECTOR.isEmpty()) {
				if (StaticConfig.TOWING_SELECTOR.size() == 1) {
					callData = Uri.parse("tel:" + StaticConfig.TOWING_SELECTOR.get(0).getNumber());
					callNow = true;
				}
				else {
					if(!HotlinePickerDialogFrag.isPickerDialogOpen()){
						pickerFragDialog = HotlinePickerDialogFrag.newInstance("Call Towing Service", StaticConfig.TOWING_SELECTOR);
						openPicker = true;
					}
				}
			}
			else if(!StaticConfig.HOTLINE_FETCHED){
				Toast.makeText(getActivity(), "Fetching hotlines.. please wait", Toast.LENGTH_SHORT).show();
			}
			break;
		case R.id.button_next:
			parentActivity.onFragmentReplaceRequest(new AccidentCritical3Fragment(), true);
			break;
		}
		
		//Check if callNow or openPicker and take action accordingly
		if (callNow && callData != null) {
			i.setData(callData);
			startActivity(i);
		}
		else if (openPicker && pickerFragDialog != null) {
			pickerFragDialog.setTargetFragment(this, REQUEST_CODE_HOTLINE_SUB_PICKER);
			pickerFragDialog.show(getActivity().getSupportFragmentManager(), "hotlinePicker");
		}
	}
}
