package fragment;


import android.content.Intent;
import android.net.Uri;

public class MyHotlineMultiSelectFragment extends MyFragment
				implements HotlinePickerDialogFrag.onSendResultListener {
	
	protected static final int REQUEST_CODE_HOTLINE_SUB_PICKER = 528; 

	@Override
	public void onSendResult(String result, int requestCode) {
		if (requestCode == REQUEST_CODE_HOTLINE_SUB_PICKER && !result.trim().equalsIgnoreCase("")) {
			Intent i = new Intent(Intent.ACTION_CALL);
			i.setData(Uri.parse("tel:" + result));
			startActivity(i);
		}
	}
}
