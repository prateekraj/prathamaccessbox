package fragment;

import com.clevertap.android.sdk.CleverTapAPI;
import com.clevertap.android.sdk.exceptions.CleverTapMetaDataNotFoundException;
import com.clevertap.android.sdk.exceptions.CleverTapPermissionsNotSatisfied;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


public class LoginBaseFragment extends Fragment {
	protected LoginReplaceListener loginListener;
	protected CleverTapAPI cleverTap = null;
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return null;
	}
	
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			cleverTap = CleverTapAPI.getInstance(getActivity());
		} catch (CleverTapMetaDataNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CleverTapPermissionsNotSatisfied e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		loginListener=(LoginReplaceListener) activity;
	}
	
	public interface LoginReplaceListener
	{
		public void onFragmentReplace(Fragment fragment,Boolean storeStack);
	}

}
