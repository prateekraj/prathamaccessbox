package db;

public class TableContract {

	/* Inner class that defines the table contents */
	public static abstract class UserDB {

		public static final String TABLE_USER_DETAILS = "user_details";
		public static final String COL_FIELD_OWN_ID = "own_id"; // int
		public static final String COL_FIELD_OWN_NUMBER = "own_num"; // int
		public static final String COL_FIELD_OWN_OTP = "one_time_password";
		public static final String COL_FIELD_ERROR = "error"; // text
		public static final String COL_FIELD_JSON_TEXT = "text"; // text
		public static final String COL_FIELD_LAST_API_CHECK = "last_api_check";
		public static final String COL_FIELD_RANDOM_HOUR = "random_hour";
		public static final String COL_FIELD_LAST_REMINDER_MESSAGE_ID = "last_reminder_message_id";
		public static final String COL_FIELD_SECOND_CHECK = "second_check";
		public static final String COL_FIELD_OWN_TOOLTIPS_CHANGESET = "tooltips_changeset";
		public static final String COL_FIELD_OWN_OFFERS_CHANGESET = "offers_changeset";
		public static final String COL_FIELD_OWN_LATEST_VERSION = "latest_version";
		public static final String COL_FIELD_OWN_UPDATE_REMINDED_FOR = "app_update_reminded_for";
		public static final String COL_FIELD_QUOTE_PHOTO_ATTEMPT_PATH = "live_quote_attempt_path";
		public static final String COL_FIELD_OWN_REFERRAL_CODE = "own_referral_code";
		public static final String COL_FIELD_OWN_CEO_CHAT_CITY_NAME = "own_ceo_chat_city_name";
        public static final String COL_FIELD_OWN_ADVISOR_CHAT_CITY_NAME = "own_advisor_chat_city_name";
		public static final String COL_FIELD_OWN_REG_NUMBER = "own_registration_number";
		public static final String COL_FIELD_OWN_REWARD_POINTS_TOTAL = "reward_points_total";
		public static final String COL_FIELD_OWN_REWARD_CHANGESET = "reward_changeset";
		public static final String COL_FIELD_EMERGENCY_CONTACT_NAME = "emg_name";
		public static final String COL_FIELD_EMERGENCY_CONTACT_NUMBER = "emg_number";
		public static final String COL_FIELD_LAST_VISITED_CAR_POSITION = "last_pos";
		public static final String COL_CAR_CHANGE_SET = "car_change_set";
		public static final String COL_FIELD_OWN_PAYMENT_CHANGESET = "payment_changeset";

	}
	public static abstract class LocationsDB {

		public static final String TABLE_LOCATIONS = "locatios_db";
		public static final String COL_LOC_NAME = "loc_name"; // int
		public static final String COL_LOC_ID = "loc_id";
		public static final String COL_LOC_SERVICE_ENABLED = "loc_service"; // int
		public static final String COL_LOC_CHAT_CEO_ENABLED = "loc_chat_ceo";
		public static final String COL_LOC_CHAT_ADVISOR_ENABLED = "loc_chat_advisor"; // text
		public static final String COL_LOC_ENQUIRY_ENABLED = "loc_enquiry"; // text
		public static final String COL_LOC_TEST_DRIVE_ENABLED = "loc_test_drive";
		public static final String COL_LOC_PAYMENT_ENABLED = "loc_payment";
		public static final String COL_LOC_CHANGE_SET = "loc_change_set";

	}

	public static abstract class CarDetailsDB {
		/*
		 * car table
		 */
		static final String TABLE_CAR_DETAILS = "cars_list";
		public static final String COL_CAR_ID = "car_id"; // int
		public static final String COL_FIELD_CAR_REGNUM = "reg_num";
		public static final String COL_FIELD_CAR_MODEL = "model"; // text
		public static final String COL_FIELD_CAR_NEXT_SERVICE_DATE = "next_service_date";
		public static final String COL_FIELD_CAR_LAST_SERVICED_AT = "service_provider";
		public static final String COL_FIELD_CAR_INSURANCE_EXPIRY = "insurance_expiry";
		public static final String COL_FIELD_CAR_INSURANCE_PROVIDER = "insurance_provider";
		public static final String COL_FIELD_CAR_PUC_EXPIRY = "puc_expiry";
		public static final String COL_FIELD_CAR_DATA_VERIFIED = "data_verified";
		public static final String COL_FIELD_CAR_PHOTO_PATH = "car_photo_path";
		public static final String COL_FIELD_CAR_PHOTO_ATTEMPT_PATH = "car_photo_attempt_path";
		public static final String COL_FIELD_CAR_INSURANCE_DOCUMENT = "insurance_document_name";
		public static final String COL_FIELD_CAR_PUC_DOCUMENT = "puc_document_name";
		public static final String COL_CAR_OWNER = "owner";
		public static final String COL_ALARM_SERVICE = "alarm_service";
		public static final String COL_ALARM_INSURANCE = "alarm_insurance";
		public static final String COL_ALARM_PUC = "alarm_puc";
		public static final String COL_LAST_SERVICE_REMINDER_CHECK = "service_reminder_check";
		public static final String COL_LAST_PUC_REMINDER_CHECK = "puc_reminder_check";
		public static final String COL_LAST_INSURANCE_REMINDER_CHECK = "insurance_reminder_check";
	}

	/*
	 * service history table
	 */
	public static abstract class ServiceDetailsDB {
		static final String TABLE_SERVICE_HISTORY = "service_history_db";
		public static final String COL_SERVICE_CAR_ID = "service_car_id"; // int
		public static final String COL_SERVICE_DATE = "service_date";
		public static final String COL_SERVICE_WORK_TYPE = "work_type"; // text
		public static final String COL_SERVICE_MILEAGE = "mileage";
		public static final String COL_SERVICE_ADVISOR = "advisor";
		public static final String COL_SERVICE_TECHNICIAN = "technician";
		public static final String COL_SERVICE_AMOUNT = "amount";
		public static final String COL_SERVICE_OUTLET = "outlet";
	}

	/*
	 * hotlines table
	 */
	public static abstract class HotlinesDB {
		public static final String TABLE_HOTLINES = "hotlines_list";
		public static final String COL_HOTLINE_ID = "hotline_id"; // int
		public static final String COL_HOTLINE_DEPARTMENT_NAME = "department_name"; // text
		public static final String COL_NAME = "label"; // text
		public static final String COL_NUMBER = "number"; // int
		public static final String COL_CHANGE_SET = "change_set";// to track
																	// change
																	// in DB

		/*
		 * hotline helper table
		 */
		public static final String TABLE_HOTLINE_HELPER = "hotline_helper";
		public static final String COL_HELPER_HOTLINE_ID = "helper_hotline_id";
		public static final String COL_HELPER_HOTLINE_DEPARTMENT_NAME = "helper_department_name";
		public static final String COL_HELPER_HOTLINE_FLAG = "helper_flag";
		public static final String COL_LOG_COUNT = "log_count";
	}
	
	
	/*
	 * chat table
	 */
	public static abstract class ChatDB{
		
		static final String TABLE_CHAT_NAME = "chat_list";
		public static final String COL_CHAT_MSG_ID = "_id"; // int
		public static final String COL_CHAT_SERVER_MSG_ID = "server_msg_id";
		public static final String COL_CHAT_MSG = "message"; // text
		public static final String COL_CHAT_SENDER = "sender"; // int
		public static final String COL_CHAT_RECEIVER = "receiver"; // int
		public static final String COL_CHAT_TYPE = "type"; // 0 = text, 1 = image
		public static final String COL_CHAT_READ_FLAG = "read_flag"; // 1 = read, 0
		public static final String COL_CHAT_DELIVERY_FLAG = "delivery_flag"; 
		public static final String COL_CHAT_SENDING_STARTED_AT = "sending_started_at"; 
		public static final String COL_CHAT_TIMESTAMP = "timestamp"; 
		public static final String COL_CHAT_CITY = "city_name"; // name of city for
		public static final String DERIVED_COL_LARGEST_MSG_ID = "largest_msg_id";			
	}
	
	//psf table detail
	public static abstract class PSFTable{
			public static final String TABLE_PSF_NAME = "psf_table"; // psf table
			public static final String COL_PSF_RATING = "psf_rate"; //psf rating
			public static final String COL_PSF_MSG_ID = "psf_msg_id";
	}
	
	public static abstract class ServiceBookingDB{
		
		public static final String TABLE_SERVICE_BOOKING = "service_booking_table";
		public static final String COL_BOOKING_ID = "service_booking_id";
		public static final String COL_CAR_ID = "car_id_booked";
		public static final String COL_BOOKING_MESSAGE_ID = "booking_msg_id";
		public static final String COL_CANCELLED_FLAG = "cancelled_flag";
		
	}
}
