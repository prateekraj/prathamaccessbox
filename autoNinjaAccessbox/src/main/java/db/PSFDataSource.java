package db;

import db.TableContract.PSFTable;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class PSFDataSource {
	// Database fields
			private SQLiteDatabase database;
			private DatabaseHelper dbHelper;
			
			public PSFDataSource(Context context) {
				dbHelper = new DatabaseHelper(context);
			}

			public void open() throws SQLException {
				database = dbHelper.getWritableDatabase();
			}
			public void close() {
				dbHelper.close();
			}
			
			public long insertPSF(ContentValues cv) {
				open();
				long a = database.insert(PSFTable.TABLE_PSF_NAME, null, cv);
				//Log.d(TAG, "insert id : " + a);
				
				close();
				return a;
			}
			
			//Post Service feedback, store the message id if rated
			public float getPsfRating(int id){
				float rating =0;
				open();
				String selection = PSFTable.COL_PSF_MSG_ID + "=?";
				String[] selectionArgs = {"" + id};
				Cursor c = database.query(PSFTable.TABLE_PSF_NAME, null, selection, selectionArgs, null, null, null);
				if(c.moveToFirst()){
					close();
					rating = c.getFloat(c.getColumnIndex(PSFTable.COL_PSF_RATING));
				}
				close();
				return rating;
				
				
				
			}

}
