package db;

import org.json.JSONException;

import config.StaticConfig;
import db.TableContract.CarDetailsDB;
import db.TableContract.UserDB;
import entity.UserDetails;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.provider.OpenableColumns;

public class UserDetailsDataSource {
	// Database fields
	private SQLiteDatabase database;
	private DatabaseHelper dbHelper;
	String selection = UserDB.COL_FIELD_OWN_NUMBER + " =?";
	public UserDetailsDataSource(Context context) {
		dbHelper = new DatabaseHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}
	public void close() {
		dbHelper.close();
	}
	/**
	 * get user last visited car position
	 * 
	 * @return
	 */
	public int getLastVisitedCarPosition() {
		open();
		String[] args = {"" + StaticConfig.LOGGED_PHONE_NUMBER};
		Cursor c = database.query(UserDB.TABLE_USER_DETAILS, null, selection, args, null, null, null);
		int position = 0;
		if (c.moveToFirst()) {
			position = c.getInt((c.getColumnIndex(UserDB.COL_FIELD_LAST_VISITED_CAR_POSITION)));
		} 
		//c.close();
		close();
		return position;

	}
	/**
	 * get user number
	 * 
	 * @return
	 */
	public long getUserOwnNumber() {
		open();
		String[] args = {"" + StaticConfig.LOGGED_PHONE_NUMBER};
		Cursor c = database.query(UserDB.TABLE_USER_DETAILS, null, selection, args, null, null, null);
		long number = 0;
		if (c.moveToFirst()) {
			number = c.getLong((c.getColumnIndex(UserDB.COL_FIELD_OWN_NUMBER)));
		} 
		//c.close();
		close();
		return number;

	}
	/**
	 * get last api check
	 * 
	 * @return
	 */
	public String getLastApiCheck() {
		open();
		String[] args = {"" + StaticConfig.LOGGED_PHONE_NUMBER};
		Cursor c = database.query(UserDB.TABLE_USER_DETAILS, null, selection, args, null, null, null);
		String lastApiCheck  = "";
		if (c.moveToFirst()) {
			lastApiCheck = c.getString((c.getColumnIndex(UserDB.COL_FIELD_LAST_API_CHECK)));
		}
		//c.close();
		close();
		return lastApiCheck;

	}
	/**
	 * get emergencyContactName(){}
	 * 
	 * @return
	 */
	public String getEmergencyContactName() {
		open();
		String[] args = {"" + StaticConfig.LOGGED_PHONE_NUMBER};
		Cursor c = database.query(UserDB.TABLE_USER_DETAILS, null, selection, args, null, null, null);
		String name  = "";
		if (c.moveToFirst()) {
			name = c.getString((c.getColumnIndex(UserDB.COL_FIELD_EMERGENCY_CONTACT_NAME)));
		}
		//c.close();
		close();
		return name;

	}
	/**
	 * get emergency number
	 * 
	 * @return
	 */
	public long getEmergencyNumber() {
		open();
		String[] args = {"" + StaticConfig.LOGGED_PHONE_NUMBER};
		Cursor c = database.query(UserDB.TABLE_USER_DETAILS, null, selection, args, null, null, null);
		long number  = 0;
		if (c.moveToFirst()) {
			number = c.getLong((c.getColumnIndex(UserDB.COL_FIELD_EMERGENCY_CONTACT_NUMBER)));
		}
		//c.close();
		close();
		return number;

	}
	/**
	 * get error
	 * 
	 * @return
	 */
	public String getError() {
		open();
		String[] args = {"" + StaticConfig.LOGGED_PHONE_NUMBER};
		Cursor c = database.query(UserDB.TABLE_USER_DETAILS, null, selection, args, null, null, null);
		String error  = "";
		if (c.moveToFirst()) {
			error = c.getString((c.getColumnIndex(UserDB.COL_FIELD_ERROR)));
		}
		//c.close();
		close();
		return error;

	}
	/**
	 * get text
	 * 
	 * @return
	 */
	public String getJsonText() {
		open();
		String[] args = {"" + StaticConfig.LOGGED_PHONE_NUMBER};
		Cursor c = database.query(UserDB.TABLE_USER_DETAILS, null, selection, args, null, null, null);
		String text  = "";
		if (c.moveToFirst()) {
			text = c.getString((c.getColumnIndex(UserDB.COL_FIELD_JSON_TEXT)));
		}
		//c.close();
		close();
		return text;

	}
	/**
	 * get latest version
	 * 
	 * @return
	 */
	public String getLatestVersion() {
		open();
		String[] args = {"" + StaticConfig.LOGGED_PHONE_NUMBER};
		Cursor c = database.query(UserDB.TABLE_USER_DETAILS, null, selection, args, null, null, null);
		String latest_version  = "0";
		if (c.moveToFirst()) {
			latest_version = c.getString((c.getColumnIndex(UserDB.COL_FIELD_OWN_LATEST_VERSION)));
		}
		//c.close();
		close();
		return latest_version;

	}
	/**
	 * get rewards total
	 * 
	 * @return
	 */
	public String getTotalRewards() {
		open();
		String[] args = {"" + StaticConfig.LOGGED_PHONE_NUMBER};
		Cursor c = database.query(UserDB.TABLE_USER_DETAILS, null, selection, args, null, null, null);
		String total_rewards  = "";
		if (c.moveToFirst()) {
			total_rewards = c.getString((c.getColumnIndex(UserDB.COL_FIELD_OWN_REWARD_POINTS_TOTAL)));
		}
		//c.close();
		close();
		return total_rewards;

	}
	/**
	 * get last reminderCheck
	 * 
	 * @return
	 */
	public String getLastRemindedFor() {
		open();
		String[] args = {"" + StaticConfig.LOGGED_PHONE_NUMBER};
		Cursor c = database.query(UserDB.TABLE_USER_DETAILS, null, selection, args, null, null, null);
		String lastRemindedFor  = "";
		if (c.moveToFirst()) {
			lastRemindedFor = c.getString((c.getColumnIndex(UserDB.COL_FIELD_OWN_UPDATE_REMINDED_FOR)));
		}
		//c.close();
		close();
		return lastRemindedFor;

	}
	/**
	 * get last ReminderMdgid
	 * 
	 * @return
	 */
	public int getReminderMsgId() {
		open();
		String[] args = {"" + StaticConfig.LOGGED_PHONE_NUMBER};
		Cursor c = database.query(UserDB.TABLE_USER_DETAILS, null, selection, args, null, null, null);
		int msgId  = 0;
		if (c.moveToFirst()) {
			msgId = c.getInt((c.getColumnIndex(UserDB.COL_FIELD_LAST_REMINDER_MESSAGE_ID)));
		}
		//c.close();
		close();
		return msgId;

	}
	/**
	 * get 
	 * 
	 * @return
	 */
	public int getRandomHr() {
		open();
		String[] args = {"" + StaticConfig.LOGGED_PHONE_NUMBER};
		Cursor c = database.query(UserDB.TABLE_USER_DETAILS, null, selection, args, null, null, null);
		int randomHr  = 0;
		if (c.moveToFirst()) {
			randomHr = c.getInt((c.getColumnIndex(UserDB.COL_FIELD_RANDOM_HOUR)));
		}
		//c.close();
		close();
		return randomHr;

	}
	/**
	 * get last Referaal code
	 * 
	 * @return
	 */
	public String getReferralCode() {
		open();
		String[] args = {"" + StaticConfig.LOGGED_PHONE_NUMBER};
		Cursor c = database.query(UserDB.TABLE_USER_DETAILS, null, selection, args, null, null, null);
		String code = "";
		if (c.moveToFirst()) {
			code = c.getString((c.getColumnIndex(UserDB.COL_FIELD_OWN_REFERRAL_CODE)));
		}
		//c.close();
		close();
		return code;

	}
	/**
	 * get last chat city name ceo
	 * 
	 * @return
	 */
	public String getCEOChatCityName() {
		open();
		String[] args = {"" + StaticConfig.LOGGED_PHONE_NUMBER};
		Cursor c = database.query(UserDB.TABLE_USER_DETAILS, null, selection, args, null, null, null);
		String city = "";
		if (c.moveToFirst()) {
			city = c.getString((c.getColumnIndex(UserDB.COL_FIELD_OWN_CEO_CHAT_CITY_NAME)));
		}
		//c.close();
		close();
		return city;

	}
	/**
	 * get last chat city name advisor
	 * 
	 * @return
	 */
	public String getAdvisorChatCityName() {
		open();
		String[] args = {"" + StaticConfig.LOGGED_PHONE_NUMBER};
		Cursor c = database.query(UserDB.TABLE_USER_DETAILS, null, selection, args, null, null, null);
		String city = "";
		if (c.moveToFirst()) {
			city = c.getString((c.getColumnIndex(UserDB.COL_FIELD_OWN_ADVISOR_CHAT_CITY_NAME)));
		}
		//c.close();
		close();
		return city;

	}
	/**
	 * get user  otp
	 * 
	 * @return
	 */
	public String getUserOtp() {
		open();
		String[] args = {"" + StaticConfig.LOGGED_PHONE_NUMBER};
		Cursor c = database.query(UserDB.TABLE_USER_DETAILS, null, selection, args, null, null, null);
		String otp = "AAAAA";
		if (c.moveToFirst()) {
			otp = c.getString((c.getColumnIndex(UserDB.COL_FIELD_OWN_OTP)));
		}
		//c.close();
		close();
		return otp;

	}
	/**
	 * get user reg num
	 * 
	 * @return
	 */
	public String getRegNum() {
		open();
		String[] args = {"" + StaticConfig.LOGGED_PHONE_NUMBER};
		Cursor c = database.query(UserDB.TABLE_USER_DETAILS, null, null, null, null, null, null);
		String reg_num = "";
		if (c.moveToFirst()) {
			reg_num = c.getString((c.getColumnIndex(UserDB.COL_FIELD_OWN_REG_NUMBER)));
		}
		//c.close();
		close();
		return reg_num;

	}
	/**
	 * update user details
	 * 
	 * @param cv
	 * @return
	 */
	public Boolean updateUserDetails(ContentValues cv) {
		open();
		String[] args = {"" + StaticConfig.LOGGED_PHONE_NUMBER};
		long a = database.update(UserDB.TABLE_USER_DETAILS, cv, selection, args);
		close();
		if (a > 0) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * 
	 * set logged in number
	 * 
	 * @param phone_number
	 * @return
	 */
	public void setLoggedInNumber() {
		open();
		 Cursor c = database.query(UserDB.TABLE_USER_DETAILS, null, null, null, null, null, null);
		if (c.moveToFirst()) {
			StaticConfig.LOGGED_PHONE_NUMBER = c.getLong(c.getColumnIndex(UserDB.COL_FIELD_OWN_NUMBER));
		}
		////c.close();
		close();
	}
	/**
	 * 
	 * to check if the input phone number exists in db or not
	 * 
	 * @param phone_number
	 * @return
	 */
	public boolean isPhoneNumberExist() {
		Boolean exists = false;
		open();
		String[] args = {"" + StaticConfig.LOGGED_PHONE_NUMBER};
		Cursor c;
		if(StaticConfig.LOGGED_PHONE_NUMBER==0){
		 c = database.query(UserDB.TABLE_USER_DETAILS, null, null, null, null, null, null);
		}else{
		 c = database.query(UserDB.TABLE_USER_DETAILS, null, selection, args, null, null, null);
		}
		if (c.moveToFirst()) {
			StaticConfig.LOGGED_PHONE_NUMBER = c.getLong(c.getColumnIndex(UserDB.COL_FIELD_OWN_NUMBER));
			exists = true;
		}
		////c.close();
		close();
		return exists;
	}
	/**
	 * 
	 * to check if emergency contact exists in db or not
	 * 
	 * @param phone_number
	 * @return
	 */
	public boolean isEmergencyContactExist() {
		Boolean exists = false;
		open();
		String[] args = {"" + StaticConfig.LOGGED_PHONE_NUMBER};
		Cursor c = database.query(UserDB.TABLE_USER_DETAILS, null, selection, args, null, null, null);
		if (c.moveToFirst() && c.getLong(c.getColumnIndex(UserDB.COL_FIELD_EMERGENCY_CONTACT_NUMBER))!=0) {
			exists = true;
		}
		////c.close();
		close();
		return exists;
	}
	/**
	 * inserting new user details
	 * 
	 * @param cv
	 * @return
	 */
	public Boolean insertUserDetails(ContentValues cv) {
		open();
		String[] args = {"" + StaticConfig.LOGGED_PHONE_NUMBER};
		boolean success = false;
		 if(database.insert(UserDB.TABLE_USER_DETAILS, null, cv)>0){
			 success = true;
		 }
		 close();
		 return success;
	}

	/**
	 * get all logged in user details
	 * 
	 * @param cv
	 * @return
	 */
	public Cursor getUserDetails() {
		open();
		String[] args = {"" + StaticConfig.LOGGED_PHONE_NUMBER};
		Cursor details = null;
		Cursor c = database.query(UserDB.TABLE_USER_DETAILS, null, selection, args, null, null, null);
		if (c.moveToFirst()) {
			details =  c;
		}
		//close();
		return details;
	}
	/**
	 * update change emergency contact
	 * @param context
	 * @param name
	 * @param number
	 * @return
	 */
	public boolean changeEmergencyContact(Context context, String name, String number) {
		    open();
		    String[] args = {"" + StaticConfig.LOGGED_PHONE_NUMBER};
			ContentValues cv = new ContentValues();
			cv.put(UserDB.COL_FIELD_EMERGENCY_CONTACT_NAME, name);
			cv.put(UserDB.COL_FIELD_EMERGENCY_CONTACT_NUMBER, number);
			updateUserDetails(cv);
			//Log.e(TAG, "JSON Exception: " + e.toString());

		return false;
	}
	/**
	 * getting the stored chnage set
	 * @return
	 */
	public int getStoredCarChangeSet() {
		open();
		String[] args = {"" + StaticConfig.LOGGED_PHONE_NUMBER};
		Cursor c = database.query(UserDB.TABLE_USER_DETAILS, null, selection, args, null, null, null);
		int changeSet = -1;
		if (c.moveToFirst()) {
			changeSet = c.getInt((c.getColumnIndex(UserDB.COL_CAR_CHANGE_SET)));
		} 
		//c.close();
		close();
		return changeSet;
	}

}
