package db;

import config.StaticConfig;
import db.TableContract.CarDetailsDB;
import entity.MyCar;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class CarDetailsDatasource {
	private SQLiteDatabase database;
	private DatabaseHelper dbHelper;

	public CarDetailsDatasource(Context context) {
		dbHelper = new DatabaseHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}
	public void close() {
		dbHelper.close();
	}
	public void setMyCarDetails() {
		// DatabaseHelper dbhelper = new
		// DatabaseHelper(getApplicationContext());
		if (StaticConfig.myCarList.size() <= 0) {
			getCarList();
		}
	}
	/**
	 * inserting the car details to db
	 * 
	 * @param cv
	 * @return
	 */
	public long insertCarDetails(ContentValues cv) {
		open();
		long a = database.insert(CarDetailsDB.TABLE_CAR_DETAILS, null, cv);
		// Log.d(TAG, "insert id : " + a);

		/*System.out.println("INSERT ALARM SERVICE: " + cv.get(CarDetailsDB.COL_ALARM_SERVICE) + ", ALARM PUC: " + cv.get(CarDetailsDB.COL_ALARM_PUC)
				+ ", ALARM insurance:" + cv.get(CarDetailsDB.COL_ALARM_INSURANCE));
		System.out
				.println("INSERT REMINDER SERVICE: " + cv.get(CarDetailsDB.COL_LAST_SERVICE_REMINDER_CHECK) + ", REMINDER PUC: "
						+ cv.get(CarDetailsDB.COL_LAST_PUC_REMINDER_CHECK) + ", REMINDER INSUARNCE:"
						+ cv.get(CarDetailsDB.COL_LAST_INSURANCE_REMINDER_CHECK));*/
		close();
		return a;
	}

	/**
	 * updating car details
	 * 
	 * @param cv
	 * @param carId
	 * @return
	 */
	public int updateCarDetails(ContentValues cv, int carId) {
		open();
		int a = database.update(CarDetailsDB.TABLE_CAR_DETAILS, cv, CarDetailsDB.COL_CAR_ID + "=?", new String[]{"" + carId});
		/*System.out.println("UPDATE ALARM SERVICE: " + cv.get(CarDetailsDB.COL_ALARM_SERVICE) + ", ALARM PUC: " + cv.get(CarDetailsDB.COL_ALARM_PUC)
				+ ", ALARM insurance:" + cv.get(CarDetailsDB.COL_ALARM_INSURANCE));
		System.out
				.println("UPDATE REMINDER SERVICE: " + cv.get(CarDetailsDB.COL_LAST_SERVICE_REMINDER_CHECK) + ", REMINDER PUC: "
						+ cv.get(CarDetailsDB.COL_LAST_PUC_REMINDER_CHECK) + ", REMINDER INSUARNCE:"
						+ cv.get(CarDetailsDB.COL_LAST_INSURANCE_REMINDER_CHECK));*/
		// Log.d(TAG, "update result: " + a + " rows | rowId : " + rowId);

		close();
		return a;
	}
	/**
	 * Method to truncate the car table for new entry
	 * 
	 * @return
	 */
	public int deleteCarDB() {
		open();
		int a = database.delete(CarDetailsDB.TABLE_CAR_DETAILS, null, null);
		// Log.d(TAG, "insert id : " + a);
		close();
		return a;
	}

	/**
	 * set Global car object
	 */
	public void setMYCarObject() {

	}
	/** setting the global car list */
	public int getCarList() {
		Cursor c = getALLCarDetails();
		StaticConfig.myCarList.clear();
		StaticConfig.myCarListNames.clear();
		int i = 0;
		if (c.moveToFirst()) {
			do {
				MyCar tempObj = new MyCar();
				tempObj.setReg_num(c.getString(c.getColumnIndex(CarDetailsDB.COL_FIELD_CAR_REGNUM)));
				tempObj.setNext_service_date(c.getString(c.getColumnIndex(CarDetailsDB.COL_FIELD_CAR_NEXT_SERVICE_DATE)));
				tempObj.setService_provider(c.getString(c.getColumnIndex(CarDetailsDB.COL_FIELD_CAR_LAST_SERVICED_AT)));
				tempObj.setService_provider(c.getString(c.getColumnIndex(CarDetailsDB.COL_FIELD_CAR_INSURANCE_EXPIRY)));
				tempObj.setInsurance_provider(c.getString(c.getColumnIndex(CarDetailsDB.COL_FIELD_CAR_INSURANCE_PROVIDER)));
				tempObj.setPuc_expiry(c.getString(c.getColumnIndex(CarDetailsDB.COL_FIELD_CAR_PUC_EXPIRY)));
				tempObj.setModelName(c.getString(c.getColumnIndex(CarDetailsDB.COL_FIELD_CAR_MODEL)));
				tempObj.setOwner(c.getString(c.getColumnIndex(CarDetailsDB.COL_CAR_OWNER)));
				tempObj.setCarId(c.getInt(c.getColumnIndex(CarDetailsDB.COL_CAR_ID)));

				//System.out.println("TEMP OBJ: CAR MODEL NAME: " + tempObj.getModelName());
				StaticConfig.myCarList.add(tempObj);
				StaticConfig.myCarListNames.add(tempObj.getModelName() + ", " + tempObj.getReg_num());
				System.out.println("MY CAR LIST SZE IS" + StaticConfig.myCarList.size());

			} while (c.moveToNext());
		} else {

		}
		close();
		if (StaticConfig.myCarList.size() > 0 && StaticConfig.position < StaticConfig.myCarList.size()) {
			StaticConfig.myCar = StaticConfig.myCarList.get(StaticConfig.position);
			// StaticConfig.position = 0;
		} else if (StaticConfig.myCarList.size() > 0 && StaticConfig.position >= StaticConfig.myCarList.size()) {
			StaticConfig.myCar = StaticConfig.myCarList.get(0);
		} else {
			StaticConfig.myCar = null;
			StaticConfig.myCar = new MyCar();
		}
		return StaticConfig.myCarList.size();

	}

	/**
	 * get the car details
	 * 
	 * @return
	 */
	public Cursor getALLCarDetails() {
		open();
		Cursor c = database.query(CarDetailsDB.TABLE_CAR_DETAILS, null, null, null, null, null, CarDetailsDB.COL_CAR_ID);
		// close();
		return c;
	}
	/**
	 * get the car service history details
	 * 
	 * @return
	 */
	public boolean regNumMathched(String regnum) {
		open();
		String selection = CarDetailsDB.COL_FIELD_CAR_REGNUM + " =?";
		String[] args = {"" + regnum};
		Cursor c = database.query(CarDetailsDB.TABLE_CAR_DETAILS, null, selection, args, null, null, null);
		Boolean regNumExist = c.moveToFirst();
		close();
		return regNumExist;
		// System.out.println("CURSOR:"+c.getString(c.getColumnIndex(DatabaseHelper.COL_SERVICE_DATE)));

	}
	/**
	 * get the car details of the given id
	 * 
	 * @return
	 */
	public Cursor getDetailsOfCarId() {
		open();
		String selection = CarDetailsDB.COL_CAR_ID + " =?";
		String[] args = {"" + StaticConfig.myCar.getCarId()};
		String result = "";
		Cursor c = database.query(CarDetailsDB.TABLE_CAR_DETAILS, null, selection, args, null, null, null);
		return c;
		/*
		 * if (c.moveToFirst()) { db.close(); return
		 * c.getString((c.getColumnIndex(COL_FIELD_CAR_PHOTO_ATTEMPT_PATH))); }
		 * else { db.close(); return result; }
		 */

	}
	/**
	 * get the car service next date
	 * 
	 * @param carID
	 * 
	 * @return
	 */
	public String getNextServiceDate(int carID) {
		open();
		String selection = CarDetailsDB.COL_CAR_ID + " =?";
		String[] args = {"" + carID};
		String result = "";
		Cursor c = database.query(CarDetailsDB.TABLE_CAR_DETAILS, null, selection, args, null, null, null);
		if (c.moveToFirst()) {
			result = c.getString((c.getColumnIndex(CarDetailsDB.COL_FIELD_CAR_NEXT_SERVICE_DATE)));
		}
		close();
		return result;

	}
	/**
	 * get the car insurance expiry date
	 * 
	 * @param carId
	 * 
	 * @return
	 */
	public String getInsuranceExpiry(int carId) {
		open();
		String selection = CarDetailsDB.COL_CAR_ID + " =?";
		String[] args = {"" + carId};
		String result = "";
		Cursor c = database.query(CarDetailsDB.TABLE_CAR_DETAILS, null, selection, args, null, null, null);
		if (c.moveToFirst()) {
			result = c.getString((c.getColumnIndex(CarDetailsDB.COL_FIELD_CAR_INSURANCE_EXPIRY)));
		}
		close();
		return result;
	}

	/**
	 * get the car Insurance doc
	 * 
	 * @return
	 */
	public String getInsuranceDocument() {
		open();
		String result = "";
		String selection = CarDetailsDB.COL_CAR_ID + " =?";
		String[] args = {"" + StaticConfig.myCar.getCarId()};
		Cursor c = database.query(CarDetailsDB.TABLE_CAR_DETAILS, null, selection, args, null, null, null);
		if (c.moveToFirst()) {
			result = c.getString((c.getColumnIndex(CarDetailsDB.COL_FIELD_CAR_INSURANCE_DOCUMENT)));
		}
		close();
		return result;
	}

	/**
	 * get the car PUC doc
	 * 
	 * @return
	 */
	public String getPUCDocument() {
		open();
		String selection = CarDetailsDB.COL_CAR_ID + " =?";
		String[] args = {"" + StaticConfig.myCar.getCarId()};
		String result = "";
		Cursor c = database.query(CarDetailsDB.TABLE_CAR_DETAILS, null, selection, args, null, null, null);
		if (c.moveToFirst()) {
			result = c.getString((c.getColumnIndex(CarDetailsDB.COL_FIELD_CAR_PUC_DOCUMENT)));
		}
		close();
		return result;
	}

	/**
	 * get the car photo attempt path
	 * 
	 * @return
	 */
	public String getPhotoAttemptPath() {
		open();
		String selection = CarDetailsDB.COL_CAR_ID + " =?";
		String[] args = {"" + StaticConfig.myCar.getCarId()};
		String result = "";
		Cursor c = database.query(CarDetailsDB.TABLE_CAR_DETAILS, null, selection, args, null, null, null);
		if (c.moveToFirst()) {
			result = c.getString((c.getColumnIndex(CarDetailsDB.COL_FIELD_CAR_PHOTO_ATTEMPT_PATH)));
		}
		close();
		return result;

	}
	/**
	 * get the car photo attempt path
	 * 
	 * @return
	 */
	public String getPhotoPath() {
		open();
		String selection = CarDetailsDB.COL_CAR_ID + " =?";
		String[] args = {"" + StaticConfig.myCar.getCarId()};
		String result = "";
		Cursor c = database.query(CarDetailsDB.TABLE_CAR_DETAILS, null, selection, args, null, null, null);
		if (c.moveToFirst()) {
			result = c.getString((c.getColumnIndex(CarDetailsDB.COL_FIELD_CAR_PHOTO_PATH)));
		}
		close();
		return result;

	}

	/**
	 * get the car puc expiry date
	 * 
	 * @param carId
	 * 
	 * @return
	 */
	public String getPucExpiry(int carId) {
		open();
		String selection = CarDetailsDB.COL_CAR_ID + " =?";
		String[] args = {"" + carId};
		String result = "";
		Cursor c = database.query(CarDetailsDB.TABLE_CAR_DETAILS, null, selection, args, null, null, null);
		if (c.moveToFirst()) {
			result = c.getString((c.getColumnIndex(CarDetailsDB.COL_FIELD_CAR_PUC_EXPIRY)));
		}
		close();
		return result;
	}
	/**
	 * get car details from reg num
	 * 
	 * @param string
	 * @return
	 */
	public int getCarDetailsFromRegNum(String string) {
		open();
		String selection = CarDetailsDB.COL_FIELD_CAR_REGNUM + " =?";
		String[] args = {string};
		int result = -1;
		Cursor c = database.query(CarDetailsDB.TABLE_CAR_DETAILS, null, selection, args, null, null, null);
		if (c.moveToFirst()) {
			result = c.getInt((c.getColumnIndex(CarDetailsDB.COL_CAR_ID)));
		}
		close();
		return result;

	}
	/**
	 * get next service alarm
	 * 
	 * @param carId
	 * @return
	 */
	public String getserviceAlarm(int carId) {
		open();
		String selection = CarDetailsDB.COL_CAR_ID + " =?";
		String[] args = {"" + carId};
		String result = "";
		Cursor c = database.query(CarDetailsDB.TABLE_CAR_DETAILS, null, selection, args, null, null, null);
		if (c.moveToFirst()) {
			result = c.getString((c.getColumnIndex(CarDetailsDB.COL_ALARM_SERVICE)));
		}
		close();
		return result;
	}
	/**
	 * get next puc alarm
	 * 
	 * @param carId
	 * @return
	 */
	public String getPucAlarm(int carId) {
		open();
		String selection = CarDetailsDB.COL_CAR_ID + " =?";
		String[] args = {"" + carId};
		String result = "";
		Cursor c = database.query(CarDetailsDB.TABLE_CAR_DETAILS, null, selection, args, null, null, null);
		if (c.moveToFirst()) {
			result = c.getString((c.getColumnIndex(CarDetailsDB.COL_ALARM_PUC)));
		}
		close();
		return result;
	}
	/**
	 * get next insurance alarm
	 * 
	 * @param carId
	 * @return
	 */
	public String getInsuranceAlarm(int carId) {
		open();
		String selection = CarDetailsDB.COL_CAR_ID + " =?";
		String[] args = {"" + carId};
		String result = "";
		Cursor c = database.query(CarDetailsDB.TABLE_CAR_DETAILS, null, selection, args, null, null, null);
		if (c.moveToFirst()) {
			result = c.getString((c.getColumnIndex(CarDetailsDB.COL_ALARM_INSURANCE)));
		}
		close();
		return result;
	}

	/**
	 * get last service reminderCheck
	 * 
	 * @return
	 */
	public String getLastServiceReminderCheck(int carId) {
		open();
		String selection = CarDetailsDB.COL_CAR_ID + " =?";
		String[] args = {"" + carId};
		String result = "";
		Cursor c = database.query(CarDetailsDB.TABLE_CAR_DETAILS, null, selection, args, null, null, null);
		if (c.moveToFirst()) {
			result = c.getString((c.getColumnIndex(CarDetailsDB.COL_LAST_SERVICE_REMINDER_CHECK)));
		}
		return result;
	}
	/**
	 * get last insurance reminderCheck
	 * 
	 * @return
	 */
	public String getLastInsuranceReminderCheck(int carId) {
		open();
		String selection = CarDetailsDB.COL_CAR_ID + " =?";
		String[] args = {"" + carId};
		String result = "";
		Cursor c = database.query(CarDetailsDB.TABLE_CAR_DETAILS, null, selection, args, null, null, null);
		if (c.moveToFirst()) {
			result = c.getString((c.getColumnIndex(CarDetailsDB.COL_LAST_INSURANCE_REMINDER_CHECK)));
		}
		close();
		return result;
	}
	/**
	 * get last puc reminderCheck
	 * 
	 * @return
	 */
	public String getLastPUCReminderCheck(int carId) {
		open();
		String selection = CarDetailsDB.COL_CAR_ID + " =?";
		String[] args = {"" + carId};
		String result = "";
		Cursor c = database.query(CarDetailsDB.TABLE_CAR_DETAILS, null, selection, args, null, null, null);
		if (c.moveToFirst()) {
			result = c.getString((c.getColumnIndex(CarDetailsDB.COL_LAST_PUC_REMINDER_CHECK)));
		}
		return result;
	}
	/**
	 * delet the car db
	 * 
	 * @param carId
	 * @return
	 */
	public int deleteACarFromDb(int carId) {
		open();
		String whereClause = CarDetailsDB.COL_CAR_ID + "=?";
		String[] whereArgs = {"" + carId};
		int a = database.delete(CarDetailsDB.TABLE_CAR_DETAILS, whereClause, whereArgs);
		close();
		return a;
	}
}
