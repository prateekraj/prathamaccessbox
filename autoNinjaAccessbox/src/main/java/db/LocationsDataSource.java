package db;

import java.util.ArrayList;

import config.ConfigInfo;
import config.StaticConfig;
import db.TableContract.HotlinesDB;
import db.TableContract.LocationsDB;
import db.TableContract.UserDB;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class LocationsDataSource {

	// Database fields
		private SQLiteDatabase database;
		private DatabaseHelper dbHelper;
		
		public LocationsDataSource(Context context) {
			dbHelper = new DatabaseHelper(context);
		}

		public void open() throws SQLException {
			database = dbHelper.getWritableDatabase();
		}
		public void close() {
			dbHelper.close();
		}
		
		/**
		 * inserting locations
		 * 
		 * @param cv
		 * @return
		 */
		public Boolean insertLocations(ContentValues cv) {
			open();
			boolean success = false;
			 if(database.insert(LocationsDB.TABLE_LOCATIONS, null, cv)>0){
				 success = true;
			 }
			 close();
			 return success;
		}
		
		/**
		 * get locations
		 * 
		 * @return
		 */
		public ArrayList<String> getLocation(String col, int enabled) {
			String selection = col + " =?";
			String[] args = {"" + enabled};
			ArrayList<String> locations = new ArrayList<String>();
			open();
			Cursor c = database.query(LocationsDB.TABLE_LOCATIONS, null, selection, args, null, null, null);
			if (c.moveToFirst()) {
				do{
				locations.add(c.getString(c.getColumnIndex(LocationsDB.COL_LOC_NAME)));
				}while(c.moveToNext());
			} 
			//c.close();
			close();
			return locations;

		}
		/**
		 * get saved location changed set
		 * @return
		 */
		public int getSavedLocChangedSet (){
			open();
			Cursor c = database.query(LocationsDB.TABLE_LOCATIONS, null, null, null, null, null, null);
			int chnged_set = 0;
			if (c.moveToFirst()) {
				chnged_set = c.getInt(c.getColumnIndex(LocationsDB.COL_LOC_CHANGE_SET));
			} 
			//c.close();
			close();
			return chnged_set;
		}
		/**
		 * Method to truncate the locations table for new entry
		 * 
		 * @return
		 */
		public long deleteLocDb() {
			open();
			long a = database.delete(LocationsDB.TABLE_LOCATIONS, null, null);
			close();
			return a;
			}
		/**
		 * get the location id from the location name
		 * @return
		 */

		public int getLocationId(String location_name) {
			// Log.d(TAG, "SelectAll!!!");
			open();
			int location_id =0;
			String selection = LocationsDB.COL_LOC_NAME + "=?";
			String[] selectionArgs = {"" + location_name};
			Cursor c = database.query(LocationsDB.TABLE_LOCATIONS, null, selection, selectionArgs, null, null, null); // retData
			if (c.moveToFirst()) {
				location_id = c.getInt(c.getColumnIndex(LocationsDB.COL_LOC_ID));
			} else {

			}
			return location_id;
		}
		/**
		 * get the location id from the location name
		 * @return
		 */

		public boolean getLocationIdsWith0() {
			// Log.d(TAG, "SelectAll!!!");
			open();
			String selection = LocationsDB.COL_LOC_ID + "=?";
			String[] selectionArgs = {"" + 0};
			Cursor c = database.query(LocationsDB.TABLE_LOCATIONS, null, selection, selectionArgs, null, null, null); // retData
			Boolean location_id = c.moveToFirst();
			return location_id;
		}
			
}

