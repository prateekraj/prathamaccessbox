package googleAnalytics;

import java.util.HashMap;
import java.util.Map;



import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;

import config.StaticConfig;
import config.ConfigInfo.Hotline;
import config.ConfigInfo.Launcher;

import entity.CallForAction;

public class GATrackerMaps {
	private static final String SCREEN_MYCAR = "My-Car";
	private static final String SCREEN_MYCAR_REPORT = "My-Car/Report";
	private static final String SCREEN_MYCAR_SEARCHING = "My-Car/Searching";
	private static final String SCREEN_MYCAR_DOCS_INSURANCE = "My-Car/Documents/Insurance";
	private static final String SCREEN_MYCAR_DOCS_PUC = "My-Car/Documents/PUC";
	private static final String SCREEN_MYCAR_SERVICE_HISTORY = "My-Car/Service-History";
	private static final String SCREEN_INBOX = "Messages";//old name for inbox retained
	private static final String SCREEN_INBOX_DETAIL = "Messages/Detail";
	private static final String SCREEN_CEO_CHAT = "Messages/CEO-Chat";
	private static final String SCREEN_LIVE_QUOTE = "Messages/Live-Quote";
	private static final String SCREEN_MESSAGES_HOME = "Messages/Home";
	private static final String SCREEN_SERVICE = "Service";
	private static final String SCREEN_HOTLINES = "Hotlines";
	private static final String SCREEN_PAYMENT = "PayNow";
	private static final String SCREEN_ACCIDENT_HOME = "Accident/Home";
	private static final String SCREEN_ACCIDENT_CRITICAL1 = "Accident/Critical/1";
	private static final String SCREEN_ACCIDENT_CRITICAL2 = "Accident/Critical/2";
	private static final String SCREEN_ACCIDENT_CRITICAL3 = "Accident/Critical/3";
	private static final String SCREEN_ACCIDENT_NONCRITICAL = "Accident/Non-Critical";
	private static final String SCREEN_OFFERS = "Offers";
	private static final String SCREEN_OFFERS_DETAIL = "Offers/Detail";
	private static final String SCREEN_TOOLTIPS = "Tooltips";
	private static final String SCREEN_TOOLTIPS_DETAIL = "Tooltips/Detail";
	private static final String SCREEN_ABOUT = "About";
	private static final String SCREEN_LOGIN = "Login";
	private static final String SCREEN_LOGIN_EDIT = "Login/EditData";
	private static final String SCREEN_HOME = "Home";
	private static final String SCREEN_REWARD = "Reward";
	private static final String SCREEN_ENQUIRY = "Enquiry";
	private static final String SCREEN_CONTACT_US = "Contact-us";
	private static final String SCREEN_PAYMENT_HISTORY = "Payment-History";
	private static final String SCREEN_CAR_ADD = "car-add";

	private static final String CATEGORY_LAUNCHER = "Launcher Buttons";
	private static final String CATEGORY_MENU = "Menu Buttons";
	private static final String CATEGORY_BACKGROUND = "Background Service";
	private static final String CATEGORY_MYCAR = "My Car Buttons";
	private static final String CATEGORY_MYCAR_DOCUMENTS = "My Car Documents";
	private static final String CATEGORY_DATES = "Important Dates";
	private static final String CATEGORY_MESSAGES = "Message Events";
	private static final String CATEGORY_OFFERS = "Offer Events";
	private static final String CATEGORY_LOCATOR = "Locator Options";
	private static final String CATEGORY_SERVICE = "Service Buttons";
	private static final String CATEGORY_HOTLINES = "Hotline Buttons";
	private static final String CATEGORY_ACCIDENT = "Accident Buttons";
	private static final String CATEGORY_REMINDER = "Reminders";
	private static final String CATEGORY_LOGIN = "Login";
	private static final String CATEGORY_NOTIFICATION = "Notification Tapped";
	private static final String CATEGORY_DIALOG = "Dialog";
	
	private static final String ACTION_CLICK = "Click"; 
	private static final String ACTION_API_RESPONSE = "API Response"; 
	private static final String ACTION_CALL = "Call"; 
	private static final String ACTION_SUBMIT = "Submit"; 
	private static final String ACTION_SET_DATE = "Set Date"; 
	private static final String ACTION_VISIT = "Visit Weblink"; 
	private static final String ACTION_RAF = "Refer a Friend"; 
	private static final String ACTION_SEND = "Send Messsage"; 
	private static final String ACTION_CHOOSE = "Choose"; 
	private static final String ACTION_NOTIFY = "Notification Created"; 
	private static final String ACTION_VIEW_REMINDER = "View Reminder"; 
	private static final String ACTION_VIEW_DOCUMENT = "View Document"; 
	private static final String ACTION_VIEW_NOTIFICATION = "View Notification"; 
	private static final String ACTION_ENQURY_SEND = "Send Enquiry";

	private static final Map<String, String> HOTLINE_LABELS_MAP = new HashMap<String, String>() {
		private static final long serialVersionUID = 1L;
		{
			put(StaticConfig.HOTLINE_ACCESSORIES, "Accessories");
			put(StaticConfig.HOTLINE_CAR_FINANCE, "Car Finance");
			put(StaticConfig.HOTLINE_CUSTOMER_CARE, "Customer Care");
			put(StaticConfig.HOTLINE_DRIVING_SCHOOL, "Driving School");
			put(StaticConfig.HOTLINE_EXTENDED_WARRANTY, "Extended Warranty");
			put(StaticConfig.HOTLINE_MOS, "MOS");
			put(StaticConfig.HOTLINE_NEW_CAR, "New Car");
			put(StaticConfig.HOTLINE_ONROAD_ASSISTANCE, "On-Road Assistance");
			put(StaticConfig.HOTLINE_RENEW_INSURANCE, "Insurance");
			put(StaticConfig.HOTLINE_RENEW_PUC, "PUC");
			put(StaticConfig.HOTLINE_SERVICE, "Service Booking");
			put(StaticConfig.HOTLINE_TOWING, StaticConfig.DEALER_FIRST_NAME + " Towing");
			put(StaticConfig.HOTLINE_USED_CAR, "Used Car Sales");//for pratham analytics, the use of label 'True Value' has been deprecated henceforth
		}
	};
	
	//View Maps
	public static final Map<String, String> 
		VIEW_MYCAR = getView(SCREEN_MYCAR),
		VIEW_MYCAR_REPORT = getView(SCREEN_MYCAR_REPORT),
		VIEW_MYCAR_SEARCHING = getView(SCREEN_MYCAR_SEARCHING),
		VIEW_MYCAR_DOCS_INSURANCE = getView(SCREEN_MYCAR_DOCS_INSURANCE),
		VIEW_MYCAR_DOCS_PUC = getView(SCREEN_MYCAR_DOCS_PUC),
		VIEW_MYCAR_SERVICE_HISTORY = getView(SCREEN_MYCAR_SERVICE_HISTORY),
		VIEW_INBOX = getView(SCREEN_INBOX),
		VIEW_INBOX_DETAIL = getView(SCREEN_INBOX_DETAIL),
		VIEW_CEO_CHAT = getView(SCREEN_CEO_CHAT),
		VIEW_LIVE_QUOTE = getView(SCREEN_LIVE_QUOTE),
		VIEW_MESSAGES_HOME = getView(SCREEN_MESSAGES_HOME),
		VIEW_SERVICE = getView(SCREEN_SERVICE),
		VIEW_HOTLINES = getView(SCREEN_HOTLINES),
		VIEW_PAYMENT = getView(SCREEN_PAYMENT),
		VIEW_ACCIDENT_HOME = getView(SCREEN_ACCIDENT_HOME),
		VIEW_ACCIDENT_CRITICAL1 = getView(SCREEN_ACCIDENT_CRITICAL1),
		VIEW_ACCIDENT_CRITICAL2 = getView(SCREEN_ACCIDENT_CRITICAL2),
		VIEW_ACCIDENT_CRITICAL3 = getView(SCREEN_ACCIDENT_CRITICAL3),
		VIEW_ACCIDENT_NONCRITICAL = getView(SCREEN_ACCIDENT_NONCRITICAL),
		VIEW_OFFERS = getView(SCREEN_OFFERS),
		VIEW_OFFERS_DETAIL = getView(SCREEN_OFFERS_DETAIL),
		VIEW_TOOLTIPS = getView(SCREEN_TOOLTIPS),
		VIEW_TOOLTIPS_DETAIL = getView(SCREEN_TOOLTIPS_DETAIL),
		VIEW_ABOUT = getView(SCREEN_ABOUT),
		VIEW_LOGIN = getView(SCREEN_LOGIN),
		VIEW_LOGIN_EDIT = getView(SCREEN_LOGIN_EDIT),
		VIEW_HOME = getView(SCREEN_HOME),
		VIEW_REWARD = getView(SCREEN_REWARD),
		VIEW_ENQUIRY = getView(SCREEN_ENQUIRY),
		VIEW_CONTACT_US = getView(SCREEN_CONTACT_US),
		VIEW_PAYMENT_HISTORY = getView(SCREEN_PAYMENT_HISTORY),
		VIEW_CAR_ADD = getView(SCREEN_CAR_ADD),

		VIEW = getEvent("", "", "", "");//dummy last line
	
	//Event Maps
	public static final Map<String, String> 
		EVENT_EDIT_DATA = getEvent(SCREEN_MYCAR, CATEGORY_MYCAR, ACTION_CLICK, "Edit Data"),
		EVENT_EDIT_MOBILE_NUMBER = getEvent(SCREEN_MYCAR, CATEGORY_MYCAR, ACTION_CLICK, "Edit Mobile Number"),
		EVENT_CAR_VIEW_HISTORY = getEvent(SCREEN_MYCAR, CATEGORY_MYCAR, ACTION_CLICK, "View Service History"),
		EVENT_CAR_CLICK_PIC =  getEvent(SCREEN_MYCAR, CATEGORY_MYCAR, ACTION_CLICK, "Snap Car Photo"),
		EVENT_CAR_SET_PIC =  getEvent(SCREEN_MYCAR, CATEGORY_MYCAR, ACTION_CLICK, "Set Car Photo"),
		EVENT_CAR_SET_PIC_FROM_GALLERY =  getEvent(SCREEN_MYCAR, CATEGORY_MYCAR, ACTION_CLICK, "Set Car Photo Gallery"),
		EVENT_CAR_NEXT_SERVICE = getEvent(SCREEN_MYCAR, CATEGORY_DATES, ACTION_CLICK, "Next Service"),
		EVENT_CAR_INSURANCE = getEvent(SCREEN_MYCAR, CATEGORY_DATES, ACTION_CLICK, "Insurance"),
		EVENT_CAR_PUC = getEvent(SCREEN_MYCAR, CATEGORY_DATES, ACTION_CLICK, "PUC"),
		EVENT_CAR_INSURANCE_VIEW_DOC = getEvent(SCREEN_MYCAR, CATEGORY_DATES, ACTION_CLICK, "Open Insurance Document Screen"),
		EVENT_CAR_PUC_VIEW_DOC = getEvent(SCREEN_MYCAR, CATEGORY_DATES, ACTION_CLICK, "Open PUC Document Screen"),
		EVENT_CAR_SET_DATE_INSURANCE = getEvent(SCREEN_MYCAR, CATEGORY_DATES, ACTION_SET_DATE, "Insurance"),
		EVENT_CAR_SET_DATE_PUC = getEvent(SCREEN_MYCAR, CATEGORY_DATES, ACTION_SET_DATE, "PUC"),
		EVENT_REPORT_PROBLEM_SUBMIT = getEvent(SCREEN_MYCAR_REPORT, CATEGORY_MYCAR, ACTION_SUBMIT, "Car Data Report - Create"),
		EVENT_EDIT_DATA_SUBMIT = getEvent(SCREEN_MYCAR_REPORT, CATEGORY_MYCAR, ACTION_SUBMIT, "Car Data Report - Edit"),
		EVENT_CAR_DOCS_EDIT_DATE_INSURANCE = getEvent(SCREEN_MYCAR_DOCS_INSURANCE, CATEGORY_DATES, ACTION_CLICK, "Insurance"),
		EVENT_CAR_DOCS_EDIT_DATE_PUC = getEvent(SCREEN_MYCAR_DOCS_PUC, CATEGORY_DATES, ACTION_CLICK, "PUC"),
		EVENT_CAR_DOCS_INSURANCE_VIEW_DOC = getEvent(SCREEN_MYCAR_DOCS_INSURANCE, CATEGORY_MYCAR_DOCUMENTS, ACTION_VIEW_DOCUMENT, "View Insurance Document"),
		EVENT_CAR_DOCS_PUC_VIEW_DOC = getEvent(SCREEN_MYCAR_DOCS_PUC, CATEGORY_MYCAR_DOCUMENTS, ACTION_VIEW_DOCUMENT, "View PUC Document"),
		EVENT_CAR_INSURANCE_CALL = getEvent(SCREEN_MYCAR_DOCS_INSURANCE, CATEGORY_HOTLINES, ACTION_CALL, "Insurance"),
		EVENT_CAR_PUC_CALL = getEvent(SCREEN_MYCAR_DOCS_PUC, CATEGORY_HOTLINES, ACTION_CALL, "PUC"),
		EVENT_DELETE_MESSAGE = getEvent(SCREEN_INBOX_DETAIL, CATEGORY_MESSAGES, ACTION_CLICK, "Delete Message"),
		EVENT_MSGHOME_OPEN_CEO_CHAT = getEvent(SCREEN_MESSAGES_HOME, CATEGORY_MESSAGES, ACTION_CLICK, "Open CEO Chat"),
		EVENT_MSGHOME_OPEN_LIVE_QUOTE = getEvent(SCREEN_MESSAGES_HOME, CATEGORY_MESSAGES, ACTION_CLICK, "Open Live Quote"),
		EVENT_MSGHOME_OPEN_INBOX = getEvent(SCREEN_MESSAGES_HOME, CATEGORY_MESSAGES, ACTION_CLICK, "Open Inbox"),
		EVENT_CEO_CHAT_SEND = getEvent(SCREEN_CEO_CHAT, CATEGORY_MESSAGES, ACTION_SEND, "Feedback to CEO"),
		EVENT_LIVE_QUOTE_SEND_TEXT = getEvent(SCREEN_LIVE_QUOTE, CATEGORY_MESSAGES, ACTION_SEND, "Message to Service Advisor"),
		EVENT_LIVE_QUOTE_CLICK_SNAP = getEvent(SCREEN_LIVE_QUOTE, CATEGORY_MESSAGES, ACTION_CLICK, "Car Damage Photo"),
		EVENT_LIVE_QUOTE_SEND_SNAP = getEvent(SCREEN_LIVE_QUOTE, CATEGORY_MESSAGES, ACTION_SEND, "Photo to Service Advisor"),
		EVENT_SERVICE_BOOK_INAPP = getEvent(SCREEN_SERVICE, CATEGORY_SERVICE, ACTION_SUBMIT, "In-App Book Service"),
		EVENT_SERVICE_BOOK_BY_CALL = getEvent(SCREEN_SERVICE, CATEGORY_HOTLINES, ACTION_CALL, HOTLINE_LABELS_MAP.get(StaticConfig.HOTLINE_SERVICE)),
		EVENT_SERVICE_ALREADY_SERVICED = getEvent(SCREEN_SERVICE, CATEGORY_SERVICE, ACTION_CLICK, "Already Serviced"),
		EVENT_SERVICE_SET_DATE_SERVICE = getEvent(SCREEN_SERVICE, CATEGORY_DATES, ACTION_SET_DATE, "Service Due"),
		EVENT_ACCIDENT_SET_EMG_CONTACT = getEvent(SCREEN_ACCIDENT_HOME, CATEGORY_ACCIDENT, ACTION_CLICK, "Set Emergency Contact"),
		EVENT_ACCIDENT_CHANGE_EMG_CONTACT = getEvent(SCREEN_ACCIDENT_HOME, CATEGORY_ACCIDENT, ACTION_CLICK, "Change Emergency Contact"),
		EVENT_ACCIDENT_CALL_EMG_CONTACT = getEvent(SCREEN_ACCIDENT_HOME, CATEGORY_ACCIDENT, ACTION_CALL, "Emergency Contact"),
		EVENT_ACCIDENT1_CALL_POLICE = getEvent(SCREEN_ACCIDENT_CRITICAL1, CATEGORY_ACCIDENT, ACTION_CALL, "Police"),
		EVENT_ACCIDENT1_LOCATE_POLICE = getEvent(SCREEN_ACCIDENT_CRITICAL1, CATEGORY_ACCIDENT, ACTION_CLICK, "Locate Police"),
		EVENT_ACCIDENT2_CALL_ONROAD_SERVICE = getEvent(SCREEN_ACCIDENT_CRITICAL2, CATEGORY_ACCIDENT, ACTION_CALL, HOTLINE_LABELS_MAP.get(StaticConfig.HOTLINE_ONROAD_ASSISTANCE)),
		EVENT_ACCIDENT2_CALL_TOWING = getEvent(SCREEN_ACCIDENT_CRITICAL2, CATEGORY_ACCIDENT, ACTION_CALL, HOTLINE_LABELS_MAP.get(StaticConfig.HOTLINE_TOWING)),
		EVENT_ABOUT_VISIT_ACCESSBOX = getEvent(SCREEN_ABOUT, CATEGORY_LAUNCHER, ACTION_VISIT, "Accessbox Weblink"),
		EVENT_LOGIN_SUBMIT_MOBILE = getEvent(SCREEN_LOGIN, CATEGORY_LOGIN, ACTION_SUBMIT, "Submit Mobile Number"),
		EVENT_LOGIN_EDIT_MOBILE = getEvent(SCREEN_LOGIN_EDIT, CATEGORY_LOGIN, ACTION_SUBMIT, "Submit Mobile Number"),
		EVENT_HOME_MORE_TIPS = getEvent(SCREEN_HOME, CATEGORY_LAUNCHER, ACTION_CLICK, "Tooltips-More"),
		EVENT_HOME_READ_FURTHER = getEvent(SCREEN_HOME, CATEGORY_LAUNCHER, ACTION_CLICK, "Tooltips-Teaser"),
		EVENT_HOME_BURGER_BUTTON = getEvent(SCREEN_HOME, CATEGORY_LAUNCHER, ACTION_CLICK, "Burger Button"),
		EVENT_HOME_LOCATE_SERVICE = getEvent(SCREEN_HOME, CATEGORY_LOCATOR, ACTION_CHOOSE, "Service Station"),
		EVENT_HOME_LOCATE_FUEL = getEvent(SCREEN_HOME, CATEGORY_LOCATOR, ACTION_CHOOSE, "Petrol Pump"),
		EVENT_HOME_LOCATE_POLICE = getEvent(SCREEN_HOME, CATEGORY_LOCATOR, ACTION_CHOOSE, "Police Station"),
		EVENT_HOME_UPDATE_APP = getEvent(SCREEN_HOME, CATEGORY_LAUNCHER, ACTION_CLICK, "Update App"),
		EVENT_HOME_UPDATE_NOT_NOW = getEvent(SCREEN_HOME, CATEGORY_LAUNCHER, ACTION_CLICK, "Update - Not Now"),
		EVENT_HOME_DEALERS_WEB_LINK = getEvent(SCREEN_HOME, CATEGORY_LAUNCHER, ACTION_CLICK, "Dealers Link"),
		EVENT_ENQUIRY_TEXT_SEND = getEvent(SCREEN_ENQUIRY, CATEGORY_MESSAGES, ACTION_ENQURY_SEND, "Enquiry to Dealer"),
		EVENT_ENQUIRY_ABOUT_DIALOG = getEvent(SCREEN_ENQUIRY, CATEGORY_DIALOG, ACTION_CHOOSE, "Enquiry About Dialog"),		
		EVENT_ENQUIRY_LOCATION_DIALOG = getEvent(SCREEN_ENQUIRY, CATEGORY_DIALOG, ACTION_CHOOSE, "Enquiry_Location_Dialog"),
		EVENT_CAR_ADD_SUBMIT = getEvent(SCREEN_CAR_ADD, CATEGORY_MYCAR, ACTION_CLICK, "Add Car Submit"),
		EVENT_CAR_ADD_CANCEL = getEvent(SCREEN_CAR_ADD, CATEGORY_MYCAR, ACTION_CLICK, "Add Car Cancel"),
		EVENT_MY_CAR_DELETE = getEvent(SCREEN_MYCAR, CATEGORY_MYCAR, ACTION_CLICK, "Delete Car"),
		EVENT_MY_CAR_DELETE_DIALOG = getEvent(SCREEN_MYCAR, CATEGORY_DIALOG, ACTION_CLICK, "Delete Car Yes/No Dialog"),
		EVENT_MY_CAR_ADD = getEvent(SCREEN_MYCAR, CATEGORY_MYCAR, ACTION_CLICK, "Go to add car"),
		
		EVENT = getEvent("", "", "", "");//dummy last line

	//get view map function
	private static Map<String, String> getView(String screenName) {
		return MapBuilder.createAppView().set(Fields.SCREEN_NAME, screenName).build();
	}

	//get event map function
	private static Map<String, String> getEvent(String screenName, String category, String action, String label) {
		return MapBuilder.createEvent(category, action, label, null)
				.set(Fields.SCREEN_NAME, screenName).build();
	}
	
	//get event map function with value
	private static Map<String, String> getEvent(String screenName, String category, String action, String label, long value) {
		return MapBuilder.createEvent(category, action, label, value)
				.set(Fields.SCREEN_NAME, screenName).build();
	}
	
	public static Map<String, String> getReminderViewEvent(String label) {
		return getEvent(SCREEN_INBOX_DETAIL, CATEGORY_REMINDER, ACTION_VIEW_REMINDER, label);
	}

	public static Map<String, String> getInboxCFAEvent(int CFAType, String label, boolean isReminder) {
		String category = CATEGORY_MESSAGES, action = "";
		switch (CFAType) {
		case CallForAction.TYPE_CALL:
			action = ACTION_CALL;
			if (isReminder) {
				category = CATEGORY_REMINDER;
			}
			break;
		case CallForAction.TYPE_WEB:
			action = ACTION_VISIT;
			break;
		case CallForAction.TYPE_REFER:
			action = ACTION_RAF;
			break;
		}
		return getEvent(SCREEN_INBOX_DETAIL, category, action, label);
	}
	
	public static Map<String, String> getServiceWorkshopChooseEvent(String workshopName) {
		return getEvent(SCREEN_SERVICE, CATEGORY_SERVICE, ACTION_CHOOSE, workshopName);
	}
	
	public static Map<String, String> getHotlineCallEvent(Hotline hotline) {
		String temp = HOTLINE_LABELS_MAP.get(hotline.getName());
		if (temp == null) {
			temp = hotline.getLabel();
		}
		return getEvent(SCREEN_HOTLINES, CATEGORY_HOTLINES, ACTION_CALL, temp);
	}

	public static Map<String, String> getOffersCFAEvent(int CFAType, String label) {
		String category = CATEGORY_OFFERS, action = "";
		switch (CFAType) {
		case CallForAction.TYPE_CALL:
			action = ACTION_CALL;
			break;
		case CallForAction.TYPE_WEB:
			action = ACTION_VISIT;
			break;
		case CallForAction.TYPE_REFER:
			action = ACTION_RAF;
			break;
		}
		return getEvent(SCREEN_OFFERS_DETAIL, category, action, label);
	}
	
	public static Map<String, String> getLaunchButtonEvent(Launcher l, boolean isMenuItem) {
		if (isMenuItem) {
			return getEvent(SCREEN_HOME, CATEGORY_LAUNCHER, ACTION_CLICK, l.getLabel());
		}
		else {
			return getEvent(SCREEN_HOME, CATEGORY_MENU, ACTION_CLICK, l.getLabel());
		}
	}
	
	public static Map<String, String> getBackgroundNotifyEvent(String label, long value) {
		return getEvent(null, CATEGORY_BACKGROUND, ACTION_NOTIFY, label, value);
	}
	
	public static Map<String, String> getBackgroundAPIEvent(String label) {
		return getEvent(null, CATEGORY_BACKGROUND, ACTION_API_RESPONSE, label);
	}
	
	public static Map<String, String> getBackgroundReminderEvent(String label) {
		return getEvent(null, CATEGORY_REMINDER, ACTION_NOTIFY, label);
	}
	
	public static Map<String, String> getNotificationViewEvent(String label) {
		return getEvent(SCREEN_HOME, CATEGORY_NOTIFICATION, ACTION_VIEW_NOTIFICATION, label);
	}
	
}
