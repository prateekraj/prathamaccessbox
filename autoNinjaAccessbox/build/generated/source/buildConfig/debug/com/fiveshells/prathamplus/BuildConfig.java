/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.fiveshells.prathamplus;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.fiveshells.prathamplus";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 52;
  public static final String VERSION_NAME = "2.5.2";
}
